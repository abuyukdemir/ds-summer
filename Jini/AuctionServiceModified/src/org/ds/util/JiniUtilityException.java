/**
 * JiniUtilityException is a class that indicates an exceptional
 * condition in one of the com.spe.jini.util classes.  This class is
 * used to wrap checked exceptions as unchecked (runtime) exceptions,
 * thereby enabling users of the Jini utilities to apply whatever
 * exception handling approach makes sense in their environment
 * without cluttering their code with try/catch blocks.
 * <br/>
 * The author recognizes that checked vs. unchecked exceptions are a
 * perennial, often heated, discussion topic among Java developers.
 * This is easily rectified by those developers who disagree with the
 * technique used here changing from their mistaken ways and recognizing
 * that the author is on the side of goodness and light.
 * <br/>
 * This source code is copyright 2008 by Patrick May.  All
 * rights reserved.
 *
 * @author Patrick May (patrick@softwarematters.org)
 * @author &copy; 2008 Patrick May.  All rights reserved.
 * @version 1
 */

package org.ds.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class JiniUtilityException extends RuntimeException
{
  /**
   * Convert an Exception to a String, preserving the stack trace
   * information.
   *
   * @param exception The Exception to convert.
   */
  private static String convertException(Exception exception)
    {
    StringWriter stringWriter = new StringWriter();
    PrintWriter printWriter = new PrintWriter(stringWriter);

    exception.printStackTrace(printWriter);
    printWriter.flush();

    return stringWriter.toString();
    }


  /**
   * The full constructor for the JiniUtilityException class.
   *
   * @param exception The Exception to uncheck.
   */
  public JiniUtilityException(Exception exception)
    {
    super(convertException(exception));
    }
}  // end JiniUtilityException
