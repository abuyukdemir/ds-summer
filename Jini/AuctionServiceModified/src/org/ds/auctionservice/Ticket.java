package org.ds.auctionservice;
import net.jini.core.entry.Entry;


public class Ticket implements Entry {
	
	public Long currentBid;
	
	public String eventName;
	
	public String venue;
	
	public String masterID;
	
	public String currentWinner;
	
	public Ticket()
	{}
	
	public Ticket(Long currentBid_, String eventName_,String venue_,String masterID_)
	{
		this.currentBid=currentBid_;
		this.eventName=eventName_;
		this.venue=venue_;
		this.currentWinner=new String();
		this.masterID=masterID_;
	}
}
