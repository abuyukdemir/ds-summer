package org.ds.auctionservice;
import java.io.IOException;
import java.io.Serializable;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.ds.auctionservice.*;
import org.ds.util.*;

import net.jini.core.discovery.LookupLocator;
import net.jini.core.entry.Entry;
import net.jini.core.entry.UnusableEntryException;
import net.jini.core.lease.Lease;
import net.jini.core.lookup.ServiceID;
import net.jini.core.lookup.ServiceItem;
import net.jini.core.lookup.ServiceMatches;
import net.jini.core.lookup.ServiceRegistrar;
import net.jini.core.lookup.ServiceTemplate;
import net.jini.core.transaction.Transaction;
import net.jini.core.transaction.TransactionException;
import net.jini.core.transaction.TransactionFactory;
import net.jini.core.transaction.server.TransactionManager;
import net.jini.discovery.LookupDiscovery;
import net.jini.discovery.LookupDiscoveryManager;
import net.jini.lease.LeaseListener;
import net.jini.lease.LeaseRenewalManager;
import net.jini.lookup.JoinManager;
import net.jini.lookup.ServiceIDListener;
import net.jini.lookup.entry.Name;
import net.jini.space.JavaSpace;


public class ServiceImp extends UnicastRemoteObject implements Service ,ServiceIDListener, Serializable
{
	
	private static ServiceID serviceID;
	public Ticket serviceTicket;
	private JavaSpace javaSpace;
	private JavaSpaceCache javaSpaceCache;
	private TransactionManager transactionManager_ = null;
	private TransactionManagerCache transactionCache_ = null;
	
	public int accountId;
	
	JiniBank bankService;
	LookupLocator lookupLocator;
	ServiceMatches services;
	public List<Integer> serviceIndexes;
	
	String firstName;
	String lastName;
	String email_addr;
	String province;
    String postalCode;
    double startCapital;
    String currency;
	


	protected ServiceImp(String firstName,String lastName, String email_addr, String province, double startCapital,String currency) throws RemoteException {
		super();
		this.firstName=firstName;
		this.lastName=lastName;
		this.email_addr=email_addr;
		this.province=province;
		this.startCapital=startCapital;
		this.currency=currency;
	}
	
	public String getServices() // It prints out the current services in a lookup service.
	{
		try {
			lookupLocator=new LookupLocator("jini://localhost"); // localhost should change depends on the computer which hosts lookup dir.
			String host = lookupLocator.getHost();
			int port = lookupLocator.getPort();
			serviceIndexes= new ArrayList<Integer>();
			
			System.out.println("Lookup Service has been found: jini://" + host+ ":" + port);
			//In this point we have found a lookup service, next step is discovery
			ServiceRegistrar registrar = lookupLocator.getRegistrar();
			ServiceTemplate serviceTemplate = new ServiceTemplate(null, null,null); // nulls is for getting all the services in the lookup after discovery
			services = registrar.lookup(serviceTemplate, 20); // Gets maximum 20 services from lookup service.
			
			for (int i = 0; i < services.totalMatches; ++i) {
				ServiceItem item = services.items[i];
				
				String name = item.service.getClass().getSimpleName();
                String id = item.serviceID.toString();
                
                if(name.equals("JiniBankService"))
                {
                	ServiceItem bank = services.items[i];
                    bankService=(JiniBank)item.service;
                }
                
                
                //System.out.println(name + ": " + id);
			}
		
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		return null; // we return the service ID of chosen service.( it should be modified)
	}
	
	private JavaSpace javaSpace()
	{
		if(javaSpace==null)
		{
			 if (javaSpaceCache == null)
			 {
				 javaSpaceCache = new JavaSpaceCache("Test");
			 }
			 javaSpace=javaSpaceCache.javaSpace();
		}
		return javaSpace;
	}
	
	public Transaction transactionGeneration()
	{
		Transaction.Created trans = null;
		
		if(transactionManager_==null)
		{
			if(transactionCache_==null)
			{
				transactionCache_= new TransactionManagerCache(new String[] {});
			}
			transactionManager_=transactionCache_.transactionManager();
		}
		try{
			trans = TransactionFactory.create(transactionManager_, 10000);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return trans.transaction;
	}

	@Override
	public boolean startBuying(int accountId,double cost) throws RemoteException {

		return bankService.purchase(this.accountId,accountId, cost);
	}
	
	public void publishTicket()
	{
		System.out.println("Service is publishing a new ticket");
		javaSpace();
		try {
			this.javaSpace.write(serviceTicket, null, 60000);
		} catch (RemoteException | TransactionException e) {
			e.printStackTrace();
		}
		System.out.println("Ticket has been written to the JavaSpace");
	}
	
	public void generateTicket(Long startingBid,String eventName,String venueName,String masterID)
	{
		serviceTicket= new Ticket(startingBid, eventName, venueName, masterID);
		System.out.println("Ticket is ready to be published");
	}

	public void serviceIDNotify (ServiceID serviceID) {
		ServiceImp.serviceID=serviceID;
    }
	
	public void checkForChange(Ticket t)
	{
		Ticket tempTicket=null;
		Ticket templateTicket=new Ticket();
		String winner = null;
		
		templateTicket.masterID=t.masterID;
		
		javaSpace();
		System.out.println("Auction has just been started. There is no bid yet.");
		
		try {
			do
		{
			Transaction transaction = transactionGeneration();
			tempTicket=null;
			tempTicket=(Ticket)javaSpace.read(templateTicket, transaction, Long.MAX_VALUE);
			winner=tempTicket.currentWinner;
			transaction.commit();
			if(tempTicket.currentBid>t.currentBid)
			{
				System.out.println("There is new bid:"
						+ tempTicket.currentBid + " from user "
						+ tempTicket.currentWinner);
				System.out.println("Current winner is "
						+ tempTicket.currentWinner);
				t=tempTicket;
			}
			}while(tempTicket!=null);
			
		} catch (RemoteException | UnusableEntryException
				| TransactionException | InterruptedException e) {
			System.out.println("Auction is over. Ticket has been sold to "
					+ winner );
		}
		
		
	}
	
	public static void main(String[] args) throws RemoteException {
		
		LookupLocator lookupLocator;
		ServiceRegistrar registrar;
		ServiceItem serviceItem;

		System.setProperty("java.rmi.server.codebase","http://localhost:8080/AuctionService.jar");
		System.setProperty("java.security.policy","file:///home/abuyukdemir/Downloads/river/examples/hello/config/policy.all");
        System.setProperty("java.rmi.server.RMIClassLoaderSpi","net.jini.loader.pref.PreferredClassProvider");
        
        System.setSecurityManager(new RMISecurityManager());
        
        Entry[] attributes = new Entry[2];
        attributes[0] = new Name("AuctionService 1");           // AUCTION NAME HERE!
        attributes[1] = new Name("200");                        // BEGINNING PRICE HERE
        try {

			ServiceImp serviceImp= new ServiceImp("Joshua","SonOfTicketSellers","steb@live.com","Antwerp",0,"EUR");
			serviceImp.getServices();
			serviceImp.accountId=serviceImp.bankService.newAccount(serviceImp);

			LookupDiscoveryManager mgr = new LookupDiscoveryManager(LookupDiscovery.ALL_GROUPS,null,null);

			//LeaseRenewalManager l= new LeaseRenewalManager(Lease.ANY, 1000000, listener)
			JoinManager joinManager = new JoinManager(serviceImp,attributes, serviceImp,mgr, new LeaseRenewalManager());
						
			Thread.sleep(1000);
			serviceImp.generateTicket((long) 200, "Rock Party", "TT Arena", ServiceImp.serviceID.toString());
			serviceImp.publishTicket();
			serviceImp.checkForChange(serviceImp.serviceTicket);
			
			
		} catch (Exception e) {
			System.out.println("AuctionService Exception:" + e);
		}
	}



}
