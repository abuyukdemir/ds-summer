package org.ds.auctionservice;
import java.rmi.*;


public interface Service {
	
	public boolean startBuying(int accountId, double cost) throws RemoteException;
	
	
}
