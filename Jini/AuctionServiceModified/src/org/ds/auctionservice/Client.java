package org.ds.auctionservice;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.ds.util.*;

import net.jini.core.discovery.LookupLocator;
import net.jini.core.entry.UnusableEntryException;
import net.jini.core.event.RemoteEventListener;
import net.jini.core.lease.Lease;
import net.jini.core.lookup.ServiceItem;
import net.jini.core.lookup.ServiceMatches;
import net.jini.core.lookup.ServiceRegistrar;
import net.jini.core.lookup.ServiceTemplate;
import net.jini.core.transaction.CannotAbortException;
import net.jini.core.transaction.CannotCommitException;
import net.jini.core.transaction.Transaction;
import net.jini.core.transaction.TransactionException;
import net.jini.core.transaction.TransactionFactory;
import net.jini.core.transaction.UnknownTransactionException;
import net.jini.core.transaction.server.TransactionManager;
import net.jini.space.JavaSpace;

public class Client {
	
	LookupLocator lookupLocator;
	Service service;
	JiniBank bankService;
	ServiceMatches services;
	
	Ticket t= null;
	///** Client info variables
	String firstName;
	String lastName;
	String email_addr;	// e-mail address
	String province;
    String postalCode;
    double startCapital;
	
	public String currency;
	
	public int accountId;
	
	
	boolean winner=false;
	public List<Integer> serviceIndexes;
	
	private TransactionManager transactionManager_ = null;
	private TransactionManagerCache transactionCache_ = null;
	private JavaSpace javaSpace;
	private JavaSpaceCache javaSpaceCache;

	public String getServices() // It prints out the current services in a lookup service.
	{
		try {
			lookupLocator=new LookupLocator("jini://localhost"); // localhost should change depends on the computer which hosts lookup dir.
			String host = lookupLocator.getHost();
			int port = lookupLocator.getPort();
			serviceIndexes= new ArrayList<Integer>();
			
			System.out.println("Lookup Service has been found: jini://" + host+ ":" + port);
			//In this point we have found a lookup service, next step is discovery
			ServiceRegistrar registrar = lookupLocator.getRegistrar();
			ServiceTemplate serviceTemplate = new ServiceTemplate(null, null,null); // nulls is for getting all the services in the lookup after discovery
			services = registrar.lookup(serviceTemplate, 20); // Gets maximum 20 services from lookup service.
			
			System.out.println("Number of services (max 20) : "+ services.totalMatches);
			
			for (int i = 0; i < services.totalMatches; ++i) {
				ServiceItem item = services.items[i];
				//services.items[i].
				
				String name = item.service.getClass().getSimpleName();
                String id = item.serviceID.toString();
                
                if(name.equals("ServiceImp"))
                {
                	serviceIndexes.add(i);
                }
                if(name.equals("JiniBankService"))
                {
                	ServiceItem bank = services.items[i];
                    bankService=(JiniBank)item.service;
                }
                
                
                System.out.println(name + ": " + id);
			}
		
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		return null; // we return the service ID of chosen service.( it should be modified)
	}
	
	private JavaSpace javaSpace()
	{
		if(javaSpace==null)
		{
			 if (javaSpaceCache == null)
			 {
				 javaSpaceCache = new JavaSpaceCache("Test");
			 }
			 javaSpace=javaSpaceCache.javaSpace();
		}
		return javaSpace;
	}
	
	private void commitTransaction(Transaction t)
	{
		try {
			t.commit();
		} catch (UnknownTransactionException | CannotCommitException
				| RemoteException e) {
			e.printStackTrace();
		}
	}
	
	private void abortTransaction(Transaction t)
	{
		try {
			t.abort();
		} catch (UnknownTransactionException | CannotAbortException
				| RemoteException e) {
			e.printStackTrace();
		}
	}
	
	public Transaction transactionGeneration()
	{
		Transaction.Created trans = null;
		
		if(transactionManager_==null)
		{
			if(transactionCache_==null)
			{
				transactionCache_= new TransactionManagerCache(new String[] {});
			}
			transactionManager_=transactionCache_.transactionManager();
		}
		try{
			trans = TransactionFactory.create(transactionManager_, 10000);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return trans.transaction;
	}
	
	
	public boolean bid(Ticket ticket,Long newBid)
	{
		Transaction transaction = null;
		Long oldBid= ticket.currentBid;
		boolean ifwritten=false;
		Ticket tempTicket=null;
		
		tempTicket=getTicket(ticket.masterID);
		if(tempTicket!=null) ticket=tempTicket;
		else return false;
		
		if(newBid>ticket.currentBid)
		{
			t.currentBid=newBid;
			t.currentWinner=firstName;
			
			transaction=transactionGeneration();
			ifwritten=writeTicket(transaction, t);
			if(ifwritten)
			{
				commitTransaction(transaction);
				System.out.println("You ("+firstName+") has just made a new bid:"+newBid+"(old bid: "+oldBid +") to the ticket with serviceID: "+ticket.masterID+" .");
				System.out.println("You are the current winner. Auction is still going on.");
				return true;
			}
			else
			{
				abortTransaction(transaction);
				return false;
			}
			
		}

		return true;
	}
	
	public Ticket waitForChange(Ticket t) {
		
		
		Ticket tempTicket=null;
		Ticket templateTicket=new Ticket();
		templateTicket.masterID=t.masterID;
		
		javaSpace();
		
		try {
			do{
				Transaction transaction = transactionGeneration();
				tempTicket=null;
				tempTicket=(Ticket)javaSpace.read(templateTicket, transaction, Long.MAX_VALUE);
				commitTransaction(transaction);
			}while(tempTicket!=null && tempTicket.currentBid<=t.currentBid);
			
		} catch (RemoteException | UnusableEntryException
				| TransactionException | InterruptedException e) {
			//e.printStackTrace();
			System.out.println("Auction is finished. Ticket object is removed from the javaspace.");
			tempTicket=null;
			
		}
		
		if(tempTicket==null)
		{
			System.out.println("You have won the auction. With the bid: "+t.currentBid);
			winner=true;
		}
		
		return tempTicket;
	}
	
	public boolean writeTicket(Transaction transaction,Ticket ticket) // 
	{
		javaSpace();
		
		Ticket temp=new Ticket();
		temp.masterID=ticket.masterID;
		Ticket test=null;
		try {
			test=(Ticket)javaSpace.take(temp, transaction, Long.MAX_VALUE);
			if(test!=null)
			{
				javaSpace.write(ticket, transaction, 60000); // Next bid in max 60 secs
				return true;
			}
			else
			{
				System.out.println("Sorry this auction has been already closed.");
				return false;
			}
			
		} catch (RemoteException | TransactionException | UnusableEntryException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public Ticket readTicket(Transaction transaction, String serviceID )
	{
		Ticket ticket=null;
		javaSpace();
		try {
			Ticket temp=new Ticket();
			temp.masterID=serviceID;
			ticket=(Ticket)javaSpace.read(temp, transaction, 10000);// 10 secs for timeout
			
		} catch (RemoteException | UnusableEntryException
				| TransactionException | InterruptedException e) {
						System.out.println("Transaction has failed to take the ticket from the JavaSpace");
						ticket=null;
		}
		return ticket;
	}
	
	public Ticket getTicket(String serviceID)
	{
		Transaction transaction = null;
		Ticket ticket = null;
		
		transaction=transactionGeneration();
		try{
			ticket = readTicket(transaction,serviceID);
			
		}catch(Exception e)
		{
			System.out.println("Ticket has expired");
		}
		if(ticket==null) return null;
		commitTransaction(transaction);
		
		return ticket;
	}
	
	
	public String selectServiceID(int serviceNo) throws RemoteException
	{

		
		ServiceItem item = services.items[serviceNo];

		String serviceName=services.items[serviceNo].attributeSets[0].toString().substring(32,services.items[serviceNo].attributeSets[0].toString().length()-1);
		String price= services.items[serviceNo].attributeSets[1].toString().substring(32,services.items[serviceNo].attributeSets[1].toString().length()-1);
        String id = item.serviceID.toString();
        service=(Service)item.service;

        System.out.println("Welcome to the "+serviceName);
		System.out.println("Beginning price is/was :" + price);
	
		return id;
	}
	
	public static void main(String[] args) throws SQLException {
		System.setProperty("java.security.policy","file:///home/abuyukdemir/Downloads/river/examples/hello/config/policy.all");
        System.setProperty("java.rmi.server.RMIClassLoaderSpi","net.jini.loader.pref.PreferredClassProvider");
        System.setSecurityManager (new RMISecurityManager());
        
        Scanner scanner=new Scanner(System.in);
        
        JiniBankService newAccount= new JiniBankService();
        Ticket changedTicket=null;
        Client c= new Client();
        Long currentBid;
        Long newBid;
        
        ////******* CREATING AN ACCOUNT IN THE BANK SERVICE DATABASE**////
        c.firstName="George";
        c.lastName="Parker";
        c.email_addr="george@live.com";
        c.province="Antwerp";
        c.startCapital=1000.0;
        c.currency="EUR";

        ////******* FINISHED TO CREATE AN ACCOUNT IN THE BANK SERVICE DATABASE***////
        c.getServices();
        
        
        try {
        	c.accountId=c.bankService.newAccount(c);
			String serviceID=c.selectServiceID(c.serviceIndexes.get(0));
			c.t=c.getTicket(serviceID);   // Here we got the ticket., Now we can bid
	        System.out.println("Current price is: "+c.t.currentBid);
	        System.out.println("In these conditions the winner will be: "+c.t.currentWinner);
	        currentBid=c.t.currentBid;
	        newBid=(long)(currentBid.intValue()+10);
	        c.bid(c.t,newBid);
	        changedTicket=null;
	        // time to read , if it changed then notify the user. User can bid.
	        changedTicket=c.waitForChange(c.t);
	        while(changedTicket!=null)
	        {
	        	boolean resume=false;
	        	System.out.println("There is a new bid:"
						+ changedTicket.currentBid + " from user: "
						+ changedTicket.currentWinner);
	        	
	        	System.out.println("You can make a new bid in 1 minute. Enter a new bid:");
		        newBid=(long) Integer.parseInt(scanner.nextLine());
		        resume=c.bid(changedTicket,newBid);
		        if(resume==false)
		        {
		        	System.out.println("You have lost the auction");
	        		return;
		        }
		        while(newBid<=changedTicket.currentBid && changedTicket!=null)
		        {
		        	System.out.println("Please make another bid. Your bid is smaller or equal to the current bid.");
		        	newBid=(long) Integer.parseInt(scanner.nextLine());
		        	resume=c.bid(changedTicket,newBid);
		        	
		        	if(resume==false)
		        	{
		        		System.out.println("You have lost the auction");
		        		return;
		        	}
		        	
		        }

		        changedTicket=null;
		        changedTicket=c.waitForChange(c.t);
	        }
	        
	        
	        
	        if(c.winner==true)
	        {
	        	String answer;
	        	System.out.println("You have won the auction. Would you like to continue the transaction?(Y/N)");
	        	while(true)
	        	{
	        		answer=scanner.nextLine();
	        	
	        		if(answer.equals("Y"))
	        		{
	        			if(c.service.startBuying(c.accountId,c.t.currentBid))
	        			{
	        				System.out.println("Transaction has been completed. You have purchased the ticket");
	        				break;
	        			}
	        			else
	        			{
	        				System.out.println("Transaction has been cancelled. You have not enough balance in your account");
	        				break;
	        			}
	        		}
	        		else if (answer.equals("N"))
	        		{
	        			System.out.println("Transaction has been cancelled");
	        			break;
	        		}
	        		else
	        		{
	        			System.out.println("Please answer Y(yes) or N(no)");
	        		}
	        	}
	        	

	        }
	        
		} catch (RemoteException e) {
			e.printStackTrace();
		}
       
        
        
        
	}

	

}
