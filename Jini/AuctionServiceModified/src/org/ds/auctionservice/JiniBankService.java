package org.ds.auctionservice;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import net.jini.core.discovery.LookupLocator;
import net.jini.core.entry.Entry;
import net.jini.core.lookup.ServiceID;
import net.jini.core.lookup.ServiceItem;
import net.jini.core.lookup.ServiceRegistrar;
import net.jini.discovery.LookupDiscovery;
import net.jini.discovery.LookupDiscoveryManager;
import net.jini.lease.LeaseRenewalManager;
import net.jini.lookup.JoinManager;
import net.jini.lookup.ServiceIDListener;
import net.jini.lookup.entry.Name;

public class JiniBankService implements JiniBank, ServiceIDListener,Serializable {
	
	public String firstName;
	
	public String lastName;
	
	public String email_addr;
	
	public String province;
	
	
	public double startCapital;
	
	public String currency;
	
	private String connectionURL;

	public JiniBankService() {
		try {
			Properties prop = new Properties();
			
			prop.load(new FileInputStream("local_config.properties"));
			String hostname = prop.getProperty("hostname");
			
			this.connectionURL = "jdbc:derby:/home/" + hostname + "/eclipse/workspace/JiniBankDatabase";
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int newAccount(Client c) throws RemoteException, SQLException {
		Connection connection = null;
		Statement insertUserStatement = null;
		Statement selectUserStatement = null;
		ResultSet resultSet = null;
		int accountId=-1;
		
		this.firstName= c.firstName;
		this.lastName= c.lastName;
		this.email_addr= c.email_addr;
		this.province =c.province;
		this.startCapital=c.startCapital;
		this.currency=c.currency;
		
		try {
			connection = DriverManager.getConnection(this.connectionURL);
			
			String insertUserQuery = "INSERT INTO bankAccounts(firstName, lastName, email_addr, province, balance, currency) VALUES " +
                    "('" + this.firstName + "', '" + this.lastName + "', '" + this.email_addr +  "', '" +
                    this.province + "'," + this.startCapital + ", '" + this.currency + "')";
			insertUserStatement = connection.createStatement();
			int numberOfRowsAffected = insertUserStatement.executeUpdate(insertUserQuery);
			assert(numberOfRowsAffected == 1);
			
			// Select user's account id from database
			String selectUserQuery = "SELECT accountId FROM bankAccounts WHERE email_addr = '" + email_addr + "'";
		    selectUserStatement = connection.createStatement();
			resultSet = selectUserStatement.executeQuery(selectUserQuery);
			resultSet.next();
			accountId = resultSet.getInt("accountId");
			
			connection.commit();
			DriverManager.getConnection(this.connectionURL+";shutdown=true");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			System.out.println("Connection Closed 1");
			if (resultSet != null) {
				resultSet.close();
				System.out.println("Connection Closed 2");
			}
			if (insertUserStatement != null) {
				insertUserStatement.close();
				System.out.println("Connection Closed 3");
			}
			if( selectUserStatement!=null){
				selectUserStatement.close();
				System.out.println("Connection Closed 4");
			}
			if (connection != null) {
				connection.close();
				System.out.println("Connection Closed 555");
				
			}
		}
		return accountId;
	}

	@Override
	public int newAccount(ServiceImp s) throws RemoteException, SQLException {
		Connection connection = null;
		Statement insertUserStatement = null;
		Statement selectUserStatement = null;
		ResultSet resultSet = null;
		int accountId=-1;
		
		this.firstName= s.firstName;
		this.lastName= s.lastName;
		this.email_addr= s.email_addr;
		this.province =s.province;
		this.startCapital=s.startCapital;
		this.currency=s.currency;
		
		try {
			connection = DriverManager.getConnection(this.connectionURL);
			
			String insertUserQuery = "INSERT INTO bankAccounts(firstName, lastName, email_addr, province, balance, currency) VALUES " +
                    "('" + this.firstName + "', '" + this.lastName + "', '" + this.email_addr +  "', '" +
                    this.province + "'," + this.startCapital + ", '" + this.currency + "')";
			insertUserStatement = connection.createStatement();
			int numberOfRowsAffected = insertUserStatement.executeUpdate(insertUserQuery);
			assert(numberOfRowsAffected == 1);
			
			// Select user's account id from database
			String selectUserQuery = "SELECT accountId FROM bankAccounts WHERE email_addr = '" + email_addr + "'";
		    selectUserStatement = connection.createStatement();
			resultSet = selectUserStatement.executeQuery(selectUserQuery);
			resultSet.next();
			accountId = resultSet.getInt("accountId");
			
			connection.commit();
			DriverManager.getConnection(this.connectionURL+";shutdown=true");

			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			System.out.println("Connection Closed 1");
			if (resultSet != null) {
				resultSet.close();
				System.out.println("Connection Closed 2");
			}
			if (insertUserStatement != null) {
				insertUserStatement.close();
				System.out.println("Connection Closed 3");
			}
			if( selectUserStatement!=null){
				selectUserStatement.close();
				System.out.println("Connection Closed 4");
			}
			if (connection != null) {
				connection.close();
				System.out.println("Connection Closed 555");
				
			}
		}
		return accountId;
	}

	@Override
	public boolean purchase(int sellerId,int buyerId, double cost) {
		Connection connection = null;
		Statement updateUserStatement = null;
		Statement selectUserStatement = null;
		ResultSet resultSet = null;
		double balance;
		
		
		try {
			connection = DriverManager.getConnection(this.connectionURL);
			
			// First update the buyer's record in the database
			String selectUserQuery = "SELECT balance FROM bankAccounts WHERE accountId = " + buyerId;
			selectUserStatement = connection.createStatement();
			resultSet = selectUserStatement.executeQuery(selectUserQuery);
			resultSet.next();
			balance = resultSet.getDouble("balance");
			
			balance= balance-cost;
			
			if(balance<0)
			{
				return false;
			}
			
			
			String updateUserQuery = "UPDATE BANKACCOUNTS set BALANCE="+balance+" where accountId="+buyerId;
			updateUserStatement=connection.createStatement();
			updateUserStatement.executeUpdate(updateUserQuery);
			
			// Now do the same operations for seller. Add money to his account.
			selectUserQuery= "SELECT balance FROM bankAccounts WHERE accountId = " + sellerId;
			selectUserStatement = connection.createStatement();
			resultSet = selectUserStatement.executeQuery(selectUserQuery);
			resultSet.next();
			balance = resultSet.getDouble("balance");
			
			balance= balance+cost;
			
			updateUserQuery = "UPDATE BANKACCOUNTS set BALANCE="+balance+" where accountId="+sellerId;
			updateUserStatement=connection.createStatement();
			updateUserStatement.executeUpdate(updateUserQuery);
			
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		LookupLocator lookupLocator;
		ServiceRegistrar registrar;
		ServiceItem serviceItem;
		
		System.setProperty("java.rmi.server.codebase","http://localhost:8080/AuctionService.jar");
		System.setProperty("java.security.policy","file:///home/abuyukdemir/Downloads/river/examples/hello/config/policy.all");
        System.setProperty("java.rmi.server.RMIClassLoaderSpi","net.jini.loader.pref.PreferredClassProvider");
        
        System.setSecurityManager(new RMISecurityManager());
        
        Entry[] attributes = new Entry[1];
        attributes[0] = new Name("JiniBankService");           // AUCTION NAME HERE!
        
		try {
			JiniBankService bankAccount= new JiniBankService();
			LookupDiscoveryManager mgr = new LookupDiscoveryManager(LookupDiscovery.ALL_GROUPS,null,null);
	        JoinManager joinManager = new JoinManager(bankAccount,attributes, bankAccount,mgr, new LeaseRenewalManager());
	        Thread.sleep(1000);
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void serviceIDNotify(ServiceID arg0) {
		// TODO Auto-generated method stub
		
	}

}
