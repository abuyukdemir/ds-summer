package org.ds.auctionservice;

import java.rmi.RemoteException;
import java.sql.SQLException;

public interface JiniBank {
	public int newAccount(Client c) throws RemoteException, SQLException;
	public int newAccount(ServiceImp s) throws RemoteException, SQLException;
	public boolean purchase(int sellerId,int buyerId, double cost);
	

}
