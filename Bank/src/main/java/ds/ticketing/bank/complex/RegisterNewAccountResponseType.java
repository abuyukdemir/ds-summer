package ds.ticketing.bank.complex;

public class RegisterNewAccountResponseType {
	
	public String ack;
	
	public int accountId;
	
	public RegisterNewAccountResponseType setAck(String ack) {
		this.ack = ack;
		return this;
	}
	
	public RegisterNewAccountResponseType setAccountId(int accountId) {
		this.accountId = accountId;
		return this;
	}
	
}
