package ds.ticketing.bank.complex;

public class GetAccountInformationResponseType {
	
	public String ack;
	
	public int accountId;
	
	public String firstName;
	
	public String lastName;
	
	public String address;
	
	public String province;
	
	public String postalCode;
	
	public double balance;
	
	public String currency;
	
	public GetAccountInformationResponseType setAck(String ack) {
		this.ack = ack;
		return this;
	}
	
	public GetAccountInformationResponseType setAccountId(int accountId) {
		this.accountId = accountId;
		return this;
	}
	
	public GetAccountInformationResponseType setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}
	
	public GetAccountInformationResponseType setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}
	
	public GetAccountInformationResponseType setAddress(String address) {
		this.address = address;
		return this;
	}
	
	public GetAccountInformationResponseType setProvince(String province) {
		this.province = province;
		return this;
	}
	
	public GetAccountInformationResponseType setPostalCode(String postalCode) {
		this.postalCode = postalCode;
		return this;
	}
	
	public GetAccountInformationResponseType setBalance(double balance) {
		this.balance = balance;
		return this;
	}
	
	public GetAccountInformationResponseType setCurrency(String currency) {
		this.currency = currency;
		return this;
	}
	
}
