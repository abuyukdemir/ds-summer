package ds.ticketing.bank.complex;

public class MakePaymentResponseType {
	
	public String ack;
	
	public MakePaymentResponseType setAck(String ack) {
		this.ack = ack;
		return this;
	}
	
}
