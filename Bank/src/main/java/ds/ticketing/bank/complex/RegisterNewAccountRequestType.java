package ds.ticketing.bank.complex;

public class RegisterNewAccountRequestType {
	
	public String firstName;
	
	public String lastName;
	
	public String address;
	
	public String province;
	
	public String postalCode;
	
	public double startCapital;
	
	public String currency;
	
}
