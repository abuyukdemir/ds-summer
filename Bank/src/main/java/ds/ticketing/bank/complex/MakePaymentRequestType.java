package ds.ticketing.bank.complex;

public class MakePaymentRequestType {
	
	public int senderId;
	
	public int receiverId;
	
	public double amount;
	
	public String currency;
	
}
