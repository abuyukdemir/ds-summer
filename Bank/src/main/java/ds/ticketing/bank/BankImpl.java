package ds.ticketing.bank;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Logger;

import javax.jws.WebMethod;
import javax.jws.WebService;

import ds.ticketing.bank.complex.GetAccountInformationRequestType;
import ds.ticketing.bank.complex.GetAccountInformationResponseType;
import ds.ticketing.bank.complex.MakePaymentRequestType;
import ds.ticketing.bank.complex.MakePaymentResponseType;
import ds.ticketing.bank.complex.RegisterNewAccountRequestType;
import ds.ticketing.bank.complex.RegisterNewAccountResponseType;
import ds.ticketing.bank.enumerate.Ack;

@WebService
public class BankImpl implements Bank {
	
	private static final Logger LOGGER = Logger.getLogger(BankImpl.class.getPackage().getName());
	
	private String connectionURL;
	private String databaseUsername;
	private String databasePassword;
	
	public BankImpl() {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		try {
			Properties prop = new Properties();
			
			prop.load(new FileInputStream("local_config.properties"));
			String hostname = prop.getProperty("hostname");
			
			this.connectionURL = "jdbc:derby:/home/" + hostname + "/eclipse/workspace/BankDatabase";
			
			prop.load(new FileInputStream("bank_config.properties"));
			this.databaseUsername = prop.getProperty("database.Username");
			this.databasePassword = prop.getProperty("database.Password");
		} catch (IOException e) {
			LOGGER.severe(e.getMessage());
		}
	}
	
	@Override
	@WebMethod
	public RegisterNewAccountResponseType registerNewAccount(RegisterNewAccountRequestType request) {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		try {
			return this.registerNewAccountWrapped(request);
		} catch (SQLException e) {
			LOGGER.severe(e.getMessage());
			return new RegisterNewAccountResponseType().setAck(Ack.ERROR.toString());
		}
	}
	
	private RegisterNewAccountResponseType registerNewAccountWrapped(RegisterNewAccountRequestType request) throws SQLException {
		RegisterNewAccountResponseType response = new RegisterNewAccountResponseType();
		
		Connection connection = null;
		Statement insertUserStatement = null;
		Statement selectUserStatement = null;
		ResultSet resultSet = null;
		
		try {
			// Set up a Derby database connection
			connection = DriverManager.getConnection(this.connectionURL, this.databaseUsername, this.databasePassword);
			
			
			// Add user to database
			String insertUserQuery = "INSERT INTO bankAccounts(firstName, lastName, address, province, postalCode, balance, currency) VALUES " +
			                         "('" + request.firstName + "', '" + request.lastName + "', '" + request.address +  "', '" +
			                         request.province + "', '" + request.postalCode + "', " + request.startCapital + ", '" + request.currency + "')";
			insertUserStatement = connection.createStatement();
			int numberOfRowsAffected = insertUserStatement.executeUpdate(insertUserQuery);
			assert(numberOfRowsAffected == 1);
			
			
			// Select user's account id from database
			String selectUserQuery = "SELECT accountId FROM bankAccounts WHERE address = '" + request.address + "'";
			selectUserStatement = connection.createStatement();
			resultSet = selectUserStatement.executeQuery(selectUserQuery);
			resultSet.next();
			int accountId = resultSet.getInt("accountId");
			
			response.setAck(Ack.SUCCESS.toString()).setAccountId(accountId);
		} catch (SQLException e) {
			LOGGER.severe(e.getMessage());
			response.setAck(Ack.ERROR.toString());
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}
			if (insertUserStatement != null) {
				insertUserStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
		
		return response;
	}
	
	@Override
	@WebMethod
	public GetAccountInformationResponseType getAccountInformation(GetAccountInformationRequestType request) {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		try {
			return this.getAccountInformationWrapped(request);
		} catch (SQLException e) {
			LOGGER.severe(e.getMessage());
			return new GetAccountInformationResponseType().setAck(Ack.ERROR.toString());
		}
	}
	
	private GetAccountInformationResponseType getAccountInformationWrapped(GetAccountInformationRequestType request) throws SQLException {
		GetAccountInformationResponseType response = new GetAccountInformationResponseType();
		
		Connection connection = null;
		Statement selectUserStatement = null;
		ResultSet resultSet = null;
		
		try {
			// Set up a Derby database connection and set auto commit to false, enabling transactions
			connection = DriverManager.getConnection(this.connectionURL, this.databaseUsername, this.databasePassword);
			
			
			String selectUserQuery = "SELECT firstName, lastName, address, province, postalCode, balance, currency FROM bankAccounts WHERE accountId = " +
			                         request.accountId;
			selectUserStatement = connection.createStatement();
			
			resultSet = selectUserStatement.executeQuery(selectUserQuery);
			
			if (resultSet.next()) {
				String firstName = resultSet.getString("firstName");
				String lastName = resultSet.getString("lastName");
				String address = resultSet.getString("address");
				String province = resultSet.getString("province");
				String postalCode = resultSet.getString("postalCode");
				double balance = resultSet.getDouble("balance");
				String currency = resultSet.getString("currency");
				
				response.setAck(Ack.SUCCESS.toString()).setFirstName(firstName).setLastName(lastName).setAddress(address).setProvince(province).
				                                        setPostalCode(postalCode).setBalance(balance).setCurrency(currency);
			} else {
				response.setAck(Ack.ACCOUNT_ID_UNKNOWN.toString());
			}
		} catch (SQLException e) {
			LOGGER.severe(e.getMessage());
			response.setAck(Ack.ERROR.toString());
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}
			if (selectUserStatement != null) {
				selectUserStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
		
		return response;
	}
	
	@Override
	@WebMethod
	public MakePaymentResponseType makePayment(MakePaymentRequestType request) {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		try {
			return this.makePaymentWrapped(request);
		} catch (SQLException e) {
			LOGGER.severe(e.getMessage());
			return new MakePaymentResponseType().setAck(Ack.ERROR.toString());
		}
	}
	
	private MakePaymentResponseType makePaymentWrapped(MakePaymentRequestType request) throws SQLException {
		MakePaymentResponseType response = new MakePaymentResponseType();
		
		Connection connection = null;
		Statement selectSenderStatement = null;
		Statement insertSenderStatement = null;
		Statement selectReceiverStatement = null;
		Statement insertReceiverStatement = null;
		ResultSet resultSet = null;
		PrintWriter printWriter = null;
		
		try {
			// Set up a Derby database connection and set auto commit to false, enabling transactions
			connection = DriverManager.getConnection(this.connectionURL, this.databaseUsername, this.databasePassword);
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(2); // connection.setTransactionIsolation(TRANSACTION_READ_COMMITTED);
			
			
			// Select sender's current balance
			selectSenderStatement = connection.createStatement();
			String selectSenderQuery = "SELECT balance FROM bankAccounts WHERE accountId = " + request.senderId;
			resultSet = selectSenderStatement.executeQuery(selectSenderQuery);
			resultSet.next();
			double oldSendersBalance = resultSet.getDouble("balance");
			
			double newSendersBalance = oldSendersBalance - request.amount;
			
			
			if (newSendersBalance >= 0 || request.senderId == request.receiverId) {
				// Withdraw money from the sender's bank account
				if (request.senderId != request.receiverId) {
					String insertSenderQuery = "UPDATE bankAccounts SET balance = " + newSendersBalance + " WHERE " +
					                           "accountId = " + request.senderId;
					insertSenderStatement = connection.createStatement();
					int numberOfRowsAffected = insertSenderStatement.executeUpdate(insertSenderQuery);
					assert(numberOfRowsAffected == 1);
				}
				
				
				// Select receiver's current balance
				String selectReceiverQuery = "SELECT balance FROM bankAccounts WHERE accountId = " + request.receiverId;
				selectReceiverStatement = connection.createStatement();
				resultSet = selectReceiverStatement.executeQuery(selectReceiverQuery);
				resultSet.next();
				double oldReceiversBalance = resultSet.getDouble("balance");
				
				double newReceiversBalance =  oldReceiversBalance + request.amount;
				
				
				// Add money to the receiver's bank account
				String insertReceiverQuery = "UPDATE bankAccounts SET balance = " + newReceiversBalance + " WHERE " +
				                             "accountId = " + request.receiverId;
				insertReceiverStatement = connection.createStatement();
				int numberOfRowsAffected = insertReceiverStatement.executeUpdate(insertReceiverQuery);
				assert(numberOfRowsAffected == 1);
				
				
				// Don't forget to commit!
				connection.commit();
				
				
				// Log the transaction to a file
				printWriter = new PrintWriter("../logs/transaction-" + new Date().getTime(), "UTF-8");
				
				printWriter.println("Date of transaction: " + new Timestamp(new Date().getTime()));
				
				GetAccountInformationResponseType getAccountInformationResponse = new GetAccountInformationResponseType();
				GetAccountInformationRequestType getAccountInformationRequest = new GetAccountInformationRequestType();
				
				if (request.senderId != request.receiverId) {
					getAccountInformationRequest.accountId = request.senderId;
					getAccountInformationResponse = this.getAccountInformation(getAccountInformationRequest);
					
					if (getAccountInformationResponse.ack.equals(Ack.SUCCESS.toString())) {
						printWriter.println("");
						printWriter.println("Sender indentification:");
						printWriter.println("\tAccount id: " + getAccountInformationResponse.accountId);
						printWriter.println("\tFirst name: " + getAccountInformationResponse.firstName);
						printWriter.println("\tLast name: " + getAccountInformationResponse.lastName);
						printWriter.println("\tAddress: " + getAccountInformationResponse.address);
						printWriter.println("\tProvince: " + getAccountInformationResponse.province);
						printWriter.println("\tPostal code: " + getAccountInformationResponse.postalCode);
						printWriter.println("\tBalance: " + oldSendersBalance + " - " +
						request.amount + " = " + getAccountInformationResponse.balance);
						printWriter.println("\tCurrency: " + getAccountInformationResponse.currency);
					}
				}
				
				getAccountInformationRequest.accountId = request.receiverId;
				getAccountInformationResponse = this.getAccountInformation(getAccountInformationRequest);
				
				if (getAccountInformationResponse.ack.equals(Ack.SUCCESS.toString())) {
					printWriter.println("");
					printWriter.println("Receiver indentification:");
					printWriter.println("\tAccount id: " + getAccountInformationResponse.accountId);
					printWriter.println("\tFirst name: " + getAccountInformationResponse.firstName);
					printWriter.println("\tLast name: " + getAccountInformationResponse.lastName);
					printWriter.println("\tAddress: " + getAccountInformationResponse.address);
					printWriter.println("\tProvince: " + getAccountInformationResponse.province);
					printWriter.println("\tPostal code: " + getAccountInformationResponse.postalCode);
					printWriter.println("\tBalance: " + oldReceiversBalance + " + " +
					request.amount + " = " + getAccountInformationResponse.balance);
					printWriter.println("\tCurrency: " + getAccountInformationResponse.currency);
				}
				
				response.setAck(Ack.SUCCESS.toString());
			} else {
				response.setAck(Ack.INSUFFICIENT_FUNDS.toString());
				
				
				// Don't forget to commit!
				connection.commit();
			}
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
			connection.rollback();
			response.setAck(Ack.ERROR.toString());
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}
			if (insertReceiverStatement != null) {
				insertReceiverStatement.close();
			}
			if (selectReceiverStatement != null) {
				selectReceiverStatement.close();
			}
			if (insertSenderStatement != null) {
				insertSenderStatement.close();
			}
			if (selectSenderStatement != null) {
				selectSenderStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
			printWriter.close();
		}
		
		return response;
	}
	
}
