package ds.ticketing.bank;

import ds.ticketing.bank.complex.GetAccountInformationRequestType;
import ds.ticketing.bank.complex.GetAccountInformationResponseType;
import ds.ticketing.bank.complex.RegisterNewAccountRequestType;
import ds.ticketing.bank.complex.RegisterNewAccountResponseType;
import ds.ticketing.bank.complex.MakePaymentRequestType;
import ds.ticketing.bank.complex.MakePaymentResponseType;

public interface Bank {
	
	public RegisterNewAccountResponseType registerNewAccount(RegisterNewAccountRequestType request);
	
	public GetAccountInformationResponseType getAccountInformation(GetAccountInformationRequestType request);
	
	public MakePaymentResponseType makePayment(MakePaymentRequestType request);
	
}
