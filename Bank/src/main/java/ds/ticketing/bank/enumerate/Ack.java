package ds.ticketing.bank.enumerate;

public enum Ack {
	
	SUCCESS("SUCCESS"),
	ACCOUNT_ID_UNKNOWN("ACCOUNT_ID_UNKNOWN"),
	INSUFFICIENT_FUNDS("INSUFFICIENT_FUNDS"),
	ERROR("ERROR");
	
	private final String name;
	
	private Ack(String name) {
		this.name = name;
	}
	
	public String toString() {
		return this.name;
	}
	
}
