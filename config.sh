#!/bin/bash

configDir=../glassfish3122eclipsedefaultdomain/config

cp local_config.properties ${configDir}
cp venue_config.properties ${configDir}
cp reseller_config.properties ${configDir}
cp organizer_config.properties ${configDir}
cp paypal_sdk_config.properties ${configDir}
cp bank_config.properties ${configDir}

cp -r EventOrganizerDatabase ..
cp -r BankDatabase ..
cp -r JiniBankDatabase
