package ds.ticketing.reseller.complex;

public class FinalizeTransactionResponseType {
	
	public String ack;
	
	public FinalizeTransactionResponseType setAck(String ack) {
		this.ack = ack;
		return this;
	}
	
}
