package ds.ticketing.reseller.complex;

public class BootstrapTicketResellerResponseType {
	
	public String ack;
	
	public BootstrapTicketResellerResponseType setAck(String ack) {
		this.ack = ack;
		return this;
	}
	
}
