package ds.ticketing.reseller.complex;

public class TransferCollectedMoneyResponseType {
	
	public String ack;
	
	public TransferCollectedMoneyResponseType setAck(String ack) {
		this.ack = ack;
		return this;
	}
	
}
