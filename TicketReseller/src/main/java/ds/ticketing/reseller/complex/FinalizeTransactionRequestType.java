package ds.ticketing.reseller.complex;

public class FinalizeTransactionRequestType {
	
	public int userId;
	
	public String[] reservedTicketIds;

	public double amount;

	public String currency;
	
	public FinalizeTransactionRequestType trim() {
		for (int i = 0; i < this.reservedTicketIds.length; ++i) {
			this.reservedTicketIds[i] = this.reservedTicketIds[i].trim();
		}
		this.currency = currency.trim();
		return this;
	}
	
}
