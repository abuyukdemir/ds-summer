package ds.ticketing.reseller;

import ds.ticketing.organizer.AbortTransactionRequestType;
import ds.ticketing.organizer.AbortTransactionResponseType;
import ds.ticketing.organizer.GetReservedTicketsInfoRequestType;
import ds.ticketing.organizer.GetReservedTicketsInfoResponseType;
import ds.ticketing.organizer.RequestToBuyRequestType;
import ds.ticketing.organizer.RequestToBuyResponseType;
import ds.ticketing.organizer.ReserveTicketsRequestType;
import ds.ticketing.organizer.ReserveTicketsResponseType;
import ds.ticketing.reseller.complex.BootstrapTicketResellerResponseType;
import ds.ticketing.reseller.complex.FinalizeTransactionRequestType;
import ds.ticketing.reseller.complex.FinalizeTransactionResponseType;
import ds.ticketing.reseller.complex.TransferCollectedMoneyResponseType;

public interface TicketReseller {
	
	public BootstrapTicketResellerResponseType bootstrapTicketReseller();
	
	public RequestToBuyResponseType requestToBuy(RequestToBuyRequestType request);
	
	public ReserveTicketsResponseType reserveTickets(ReserveTicketsRequestType request);
	
	public GetReservedTicketsInfoResponseType getReservedTicketsInfo(GetReservedTicketsInfoRequestType request);
	
	public AbortTransactionResponseType abortTransaction(AbortTransactionRequestType request);
	
	public FinalizeTransactionResponseType finalizeTransaction(FinalizeTransactionRequestType request);
	
	public TransferCollectedMoneyResponseType transferCollectedMoney();
	
}