package ds.ticketing.reseller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Properties;
import java.util.logging.Logger;

import javax.jws.WebMethod;
import javax.jws.WebService;

import ds.ticketing.bank.BankImpl;
import ds.ticketing.bank.BankImplProxy;
import ds.ticketing.bank.GetAccountInformationRequestType;
import ds.ticketing.bank.GetAccountInformationResponseType;
import ds.ticketing.bank.MakePaymentRequestType;
import ds.ticketing.bank.MakePaymentResponseType;
import ds.ticketing.bank.RegisterNewAccountRequestType;
import ds.ticketing.bank.RegisterNewAccountResponseType;
import ds.ticketing.organizer.AbortTransactionRequestType;
import ds.ticketing.organizer.AbortTransactionResponseType;
import ds.ticketing.organizer.EventOrganizerImpl;
import ds.ticketing.organizer.EventOrganizerImplProxy;
import ds.ticketing.organizer.GetReservedTicketsInfoRequestType;
import ds.ticketing.organizer.GetReservedTicketsInfoResponseType;
import ds.ticketing.organizer.RequestPaymentInformationResponseType;
import ds.ticketing.organizer.RequestToBuyRequestType;
import ds.ticketing.organizer.RequestToBuyResponseType;
import ds.ticketing.organizer.ReserveTicketsRequestType;
import ds.ticketing.organizer.ReserveTicketsResponseType;
import ds.ticketing.reseller.complex.BootstrapTicketResellerResponseType;
import ds.ticketing.reseller.complex.FinalizeTransactionRequestType;
import ds.ticketing.reseller.complex.FinalizeTransactionResponseType;
import ds.ticketing.reseller.complex.TransferCollectedMoneyResponseType;
import ds.ticketing.reseller.enumerate.Ack;

@WebService
public class TicketResellerImpl implements TicketReseller {
	
	private static final Logger LOGGER = Logger.getLogger(TicketReseller.class.getPackage().getName());
	
	private EventOrganizerImplProxy eventOrganizerImplProxy = new EventOrganizerImplProxy();
	private EventOrganizerImpl eventOrganizerImpl = eventOrganizerImplProxy.getEventOrganizerImpl();
	
	@Override
	@WebMethod
	public BootstrapTicketResellerResponseType bootstrapTicketReseller() {
		BootstrapTicketResellerResponseType response = new BootstrapTicketResellerResponseType();
		
		try {
			// Read event organizer information from properties file
			Properties prop = new Properties();
			prop.load(new FileInputStream("reseller_config.properties"));
			String name = prop.getProperty("name");
			String address = prop.getProperty("address");
			String province = prop.getProperty("province");
			String postalCode = prop.getProperty("postalCode");
			double startCapital = Double.parseDouble(prop.getProperty("bankAccount.StartCapital"));
			String currency = prop.getProperty("bankAccount.Currency");
			
			RegisterNewAccountRequestType registerNewAccountRequest = new RegisterNewAccountRequestType();
			registerNewAccountRequest.setFirstName(name);
			registerNewAccountRequest.setLastName("Reseller");
			registerNewAccountRequest.setAddress(address);
			registerNewAccountRequest.setProvince(province);
			registerNewAccountRequest.setPostalCode(postalCode);
			registerNewAccountRequest.setStartCapital(startCapital);
			registerNewAccountRequest.setCurrency(currency);
			
			BankImplProxy bankImplProxy = new BankImplProxy();
			BankImpl bankImpl = bankImplProxy.getBankImpl();
			
			RegisterNewAccountResponseType registerNewAccountResponse = bankImpl.registerNewAccount(registerNewAccountRequest);
			
			if (registerNewAccountResponse.getAck().equals(Ack.SUCCESS.toString())) {
				prop.setProperty("bankAccount.Id", Integer.toString(registerNewAccountResponse.getAccountId()));
				prop.store(new FileOutputStream("reseller_config.properties"), null);
				
				response.setAck(Ack.SUCCESS.toString());
			} else {
				response.setAck(Ack.ERROR.toString());
			}
			
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			response.setAck(Ack.ERROR.toString());
			return response;
		}
	}
	
	@Override
	@WebMethod
	public RequestToBuyResponseType requestToBuy(RequestToBuyRequestType request) {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		try {
			return this.eventOrganizerImpl.requestToBuy(request);
		} catch (RemoteException e) {
			LOGGER.severe(e.getMessage());
			RequestToBuyResponseType response = new RequestToBuyResponseType();
			response.setAck(Ack.ERROR.toString());
			return response;
		}
	}
	
	@Override
	@WebMethod
	public ReserveTicketsResponseType reserveTickets(ReserveTicketsRequestType request) {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		try {
			return this.eventOrganizerImpl.reserveTickets(request);
		} catch (RemoteException e) {
			LOGGER.severe(e.getMessage());
			ReserveTicketsResponseType response = new ReserveTicketsResponseType();
			response.setAck(Ack.ERROR.toString());
			return response;
		}
	}
	
	@Override
	@WebMethod
	public GetReservedTicketsInfoResponseType getReservedTicketsInfo(GetReservedTicketsInfoRequestType request) {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		try {
			return this.eventOrganizerImpl.getReservedTicketsInfo(request);
		} catch (RemoteException e) {
			LOGGER.severe(e.getMessage());
			GetReservedTicketsInfoResponseType response = new GetReservedTicketsInfoResponseType();
			response.setAck(Ack.ERROR.toString());
			return response;
		}
	}
	
	@Override
	@WebMethod
	public AbortTransactionResponseType abortTransaction(AbortTransactionRequestType request) {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		try {
			return this.eventOrganizerImpl.abortTransaction(request);
		} catch (RemoteException e) {
			LOGGER.severe(e.getMessage());
			AbortTransactionResponseType response = new AbortTransactionResponseType();
			response.setAck(Ack.ERROR.toString());
			return response;
		}
	}
	
	@Override
	@WebMethod
	public FinalizeTransactionResponseType finalizeTransaction(FinalizeTransactionRequestType request) {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		FinalizeTransactionResponseType response = new FinalizeTransactionResponseType();
		
		try {
			ds.ticketing.organizer.FinalizeTransactionRequestType finalizeTransactionRequest = new ds.ticketing.organizer.FinalizeTransactionRequestType();
			finalizeTransactionRequest.setUserId(request.userId);
			finalizeTransactionRequest.setReservedTicketIds(request.reservedTicketIds);
			ds.ticketing.organizer.FinalizeTransactionResponseType finalizeTransactionResponse = this.eventOrganizerImpl.finalizeTransaction(finalizeTransactionRequest);
			
			if (finalizeTransactionResponse.getAck().equals(Ack.SUCCESS.toString())) {
				BankImplProxy bankImplProxy = new BankImplProxy();
				BankImpl bankImpl = bankImplProxy.getBankImpl();
				
				Properties prop = new Properties();
				prop.load(new FileInputStream("reseller_config.properties"));
				int senderId = Integer.parseInt(prop.getProperty("bankAccount.Id"));
				int receiverId = senderId;
				
				MakePaymentRequestType makePaymentRequest = new MakePaymentRequestType();
				makePaymentRequest.setSenderId(senderId);
				makePaymentRequest.setReceiverId(receiverId);
				makePaymentRequest.setAmount(request.amount);
				makePaymentRequest.setCurrency(request.currency);
				
				MakePaymentResponseType makePaymentResponse = bankImpl.makePayment(makePaymentRequest);
				
				if (makePaymentResponse.getAck().equals(Ack.SUCCESS.toString())) {
					response.setAck(Ack.SUCCESS.toString());
				} else {
					response.setAck(Ack.ERROR.toString());
				}
			} else {
				response.setAck(Ack.ERROR.toString());
			}
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
			response.setAck(Ack.ERROR.toString());
		}
		
		return response;
	}
	
	@Override
	@WebMethod
	public TransferCollectedMoneyResponseType transferCollectedMoney() {
		TransferCollectedMoneyResponseType response = new TransferCollectedMoneyResponseType();
		
		try {
			EventOrganizerImplProxy eventOrganizerImplProxy = new EventOrganizerImplProxy();
			EventOrganizerImpl eventOrganizerImpl = eventOrganizerImplProxy.getEventOrganizerImpl();
			
			RequestPaymentInformationResponseType requestPaymentInformationResponse = eventOrganizerImpl.requestPaymentInformation();
			
			if (requestPaymentInformationResponse.getAck().equals(Ack.SUCCESS.toString())) {
				Properties prop = new Properties();
				prop.load(new FileInputStream("reseller_config.properties"));
				int sendersAccountId = Integer.parseInt(prop.getProperty("bankAccount.Id"));
				int receiversAccountId = requestPaymentInformationResponse.getAccountId();
				
				GetAccountInformationRequestType getAccountInformationRequest = new GetAccountInformationRequestType();
				getAccountInformationRequest.setAccountId(sendersAccountId);
				
				BankImplProxy bankImplProxy = new BankImplProxy();
				BankImpl bankImpl = bankImplProxy.getBankImpl();
				
				GetAccountInformationResponseType getAccountInformationResponse = bankImpl.getAccountInformation(getAccountInformationRequest);
				
				double amount = getAccountInformationResponse.getBalance()*(1.00 - requestPaymentInformationResponse.getResellerFee());
				
				if (getAccountInformationResponse.getAck().equals(Ack.SUCCESS.toString())) {
					MakePaymentRequestType makePaymentRequest = new MakePaymentRequestType();
					makePaymentRequest.setSenderId(sendersAccountId);
					makePaymentRequest.setReceiverId(receiversAccountId);
					makePaymentRequest.setAmount(amount);
					makePaymentRequest.setCurrency(getAccountInformationResponse.getCurrency());
					
					bankImpl.makePayment(makePaymentRequest);
					
					response.setAck(Ack.SUCCESS.toString());
				} else {
					response.setAck(Ack.ERROR.toString());
				}
			} else {
				response.setAck(Ack.ERROR.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setAck(Ack.ERROR.toString());
		}
		
		return response;
	}
	
}
