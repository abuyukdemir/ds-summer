<%@page contentType="text/html;charset=UTF-8"%>

<html lang="en">
	<head>
		<title>Ticketing Service</title>
		<!-- Meta data -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Distributed Computing: Ticketing Service">
		<meta name="author" content="Ahmet Can Büyükdemir and Chennara Nhes">
		<!-- CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="css/sticky-footer.min.css" rel="stylesheet" media="screen">
		<link href="css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
		<link href="css/error-classes.min.css" rel="stylesheet" media="screen">
		<!-- JavaScript -->
		<script src="js/jquery.min.js" type="text/javascript"></script>
	</head>
	<body>
		<div id="wrap">
			<!-- Begin page content -->
			<div class="container">
				<div class="page-header">
					<h1>Ticketing Service<img src="img/golden-ticket.png" width=50 height=50 align="right"></h1>
				</div>
				<!-- Progress bar -->
				<div id="progressBarDiv" class="container">
					<h3>Progress</h3>
					<div class="progress progress-info progress-striped active">
						<div id="progressBar" class="bar" style="width: 0%"></div>
					</div>
				</div>
				<br id="break1">
				<!-- Request to buy section -->
				<div id="requestToBuyDiv" class="container">
					<h3>User Information</h3>
					<form id="requestToBuyForm" class="form-horizontal" method="post">
						<div class="control-group">
							<label id="inputFirstNameLabel" class="control-label" for="inputFirstName">First Name*</label>
							<div class="controls">
								<input type="text" name="inputFirstName" id="inputFirstName" placeholder="First Name">
							</div>
						</div>
						<div class="control-group">
							<label id="inputLastNameLabel" class="control-label" for="inputLastName">Last Name*</label>
							<div class="controls">
								<input type="text" name="inputLastName" id="inputLastName" placeholder="Last Name">
							</div>
						</div>
						<div class="control-group">
							<label id="inputEmailAddressLabel" class="control-label" for="inputEmailAddress">Email Address*</label>
							<div class="controls">
								<input type="text" name="inputEmailAddress" id="inputEmailAddress" placeholder="Email Address">
							</div>
						</div>
						<div class="control-group">
							<label id="inputProvinceLabel" class="control-label" for="inputProvince">Province*</label>
							<div class="controls">
								<select name="inputProvince" id="inputProvince">
									<option value="Antwerp">Antwerp</option>
									<option value="East Flanders">East Flanders</option>
									<option value="Flemish Brabant">Flemish Brabant</option>
									<option value="Hainaut">Hainaut</option>
									<option value="Limburg">Limburg</option>
									<option value="Liège">Liège</option>
									<option value="Luxembourg">Luxembourg</option>
									<option value="Namur">Namur</option>
									<option value="Walloon Brabant">Walloon Brabant</option>
									<option value="West Flanders">West Flanders</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<div class="controls">
								<button id="requestToBuyButton" type="submit" class="btn">Submit</button>
							</div>
						</div>
					</form>
					<script src="js/jquery-validate.min.js" type="text/javascript"></script>
					<script src="js/ticket-reseller/request-to-buy.js" type="text/javascript"></script>
				</div>
				<br id="break2">
				<!-- Reserve tickets section -->
				<div id="reserveTicketsDiv" class="container">
				</div>
				<br id="break3">
				<!-- Make payment section -->
				<div id="makePaymentDiv" class="container">
				</div>
			</div>
			<!-- Push for sticky footer -->
			<div id="push">
			</div>
		</div>
		<!-- Footer -->
		<div id="footer">
			<div class="container">
				<p class="muted credit">Ticketing Service &copy; 2013 &sdot; Ahmet Can Büyükdemir and Chennara Nhes</p>
			</div>
		</div>
	</body>
</html>
