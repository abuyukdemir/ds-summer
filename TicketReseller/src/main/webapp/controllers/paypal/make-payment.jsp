<%@ page import="ds.ticketing.frontend.paypal.*" %>
<%@ page import="java.io.FileInputStream" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Properties" %>

<%
SetExpressCheckoutRequestType setExpressCheckoutRequest = new SetExpressCheckoutRequestType();

String piggyback = "?userId=" + request.getParameter("inputUserId").trim() + "&";

Map<String, String[]> parameters = request.getParameterMap();
for (String parameter : parameters.keySet()) {
	if (parameter.startsWith("inputTicketId")) {
		assert(parameters.get(parameter).length == 1);
		
		int inputTicketId = Integer.parseInt(parameters.get(parameter)[0].trim());
		piggyback += "reservedTicketId=" + parameters.get(parameter)[0].trim() + "&";
		
		String inputTicketEventName = request.getParameter("inputTicketEventName" + parameters.get(parameter)[0]);
		String inputTicketVenueName = request.getParameter("inputTicketVenueName" + parameters.get(parameter)[0]);
		String inputTicketSeatNumber = request.getParameter("inputTicketSeatNumber" + parameters.get(parameter)[0]);
		String inputTicketPrice = request.getParameter("inputTicketPrice" + parameters.get(parameter)[0]);
		String inputTicketCurrency = request.getParameter("inputTicketCurrency" + parameters.get(parameter)[0]);
		
		TicketInfo ticketInfo = new TicketInfo();
		ticketInfo.setId(inputTicketId);
		ticketInfo.setEventName(inputTicketEventName);
		ticketInfo.setVenueName(inputTicketVenueName);
		ticketInfo.setSeatNumber(inputTicketSeatNumber);
		ticketInfo.setPrice(Double.parseDouble(inputTicketPrice));
		ticketInfo.setCurrency(inputTicketCurrency);
		
		setExpressCheckoutRequest.getReservedTickets().add(ticketInfo);
	}
}

piggyback = piggyback.substring(0, piggyback.length() - 1);

Properties prop = new Properties();
prop.load(new FileInputStream("local_config.properties"));
String ipAddress = prop.getProperty("ipAddress");

String returnURL = "http://" + ipAddress + ":8080/TicketReseller/payment-success.jsp" + piggyback;
String cancelURL = "http://" + ipAddress + ":8080/TicketReseller/payment-fail.jsp" + piggyback;

setExpressCheckoutRequest.setReturnURL(returnURL);
setExpressCheckoutRequest.setCancelURL(cancelURL);

ExpressCheckoutImplService expressCheckoutImplService = new ExpressCheckoutImplService();
ExpressCheckoutImpl expressCheckoutImpl = expressCheckoutImplService.getExpressCheckoutImplPort();
SetExpressCheckoutResponseType setExpressCheckoutResponse = expressCheckoutImpl.setExpressCheckout(setExpressCheckoutRequest);

String expressCheckoutToken = setExpressCheckoutResponse.getToken();

response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
response.setHeader("Location", "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=" + expressCheckoutToken);
%>
