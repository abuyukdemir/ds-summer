$(document).ready(function() {
	$("#progressBar").attr("style", "width: 66.67%");
	$(".ticketCheckbox input:checked").parent().replaceWith("<i class=\"icon-ok\"></i>");
	$(".ticketCheckbox").parent().parent().remove();
	$("#reserveTicketsButton").remove();
});
