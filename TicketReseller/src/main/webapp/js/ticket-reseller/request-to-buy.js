$(document).ready(function() {
	$("#requestToBuyForm").validate({
		debug: false,
		rules: {
			inputFirstName: "required",
			inputLastName: "required",
			inputEmailAddress: {
				required: true,
				email: true
			}
		},
		messages: {
			inputFirstName: "First name is required.",
			inputLastName: "Last name is required.",
			inputEmailAddress: {
				required: "Email address is required.",
				email: "A valid email address is required."
			}
		},
		submitHandler: function(form) {
			$.post("views/ticket-reseller/request-to-buy.jsp", $("#requestToBuyForm").serialize(), function(data) {
				$("#progressBar").attr("style", "width: 33.33%");
				$("#inputFirstNameLabel").text($("#inputFirstNameLabel").text().replace("*",""));
				$("#inputLastNameLabel").text($("#inputLastNameLabel").text().replace("*",""));
				$("#inputEmailAddressLabel").text($("#inputEmailAddressLabel").text().replace("*",""));
				$("#inputProvinceLabel").text($("#inputProvinceLabel").text().replace("*",""));
				$("#inputFirstName").replaceWith("<span class=\"uneditable-input\">" + $("#inputFirstName").val() + "</span>");
				$("#inputLastName").replaceWith("<span class=\"uneditable-input\">" + $("#inputLastName").val() + "</span>");
				$("#inputEmailAddress").replaceWith("<span class=\"uneditable-input\">" + $("#inputEmailAddress").val() + "</span>");
				$("#inputProvince").replaceWith("<span class=\"uneditable-input\">" + $("#inputProvince").val() + "</span>");
				$("#requestToBuyButton").remove();
				$("#reserveTicketsDiv").html(data);
			});
		}
	});
});
