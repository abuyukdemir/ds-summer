$("#reserveTicketsForm").submit(function(event) {
	event.preventDefault();
	if ($("#reserveTicketsForm input:checked").length == 0) {
		$("#errorReserveTicketsForm").html("Oops, you should check out at least one ticket.");
	} else {
		$("#errorReserveTicketsForm").html(""); // TODO: Remove error message already when one ticket was checked
		$.post("views/ticket-reseller/reserve-tickets.jsp", $("#reserveTicketsForm").serialize(), function(data) {
			$("#makePaymentDiv").html(data);
		});
	}
});
