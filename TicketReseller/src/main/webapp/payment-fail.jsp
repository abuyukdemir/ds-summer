<%@page contentType="text/html;charset=UTF-8"%>

<%@ page import="ds.ticketing.frontend.paypal.*" %>
<%@ page import="ds.ticketing.frontend.reseller.*" %>
<%@ page import="java.io.FileInputStream" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Properties" %>

<%
List<String> reservedTicketIds = new LinkedList<String>();
Map<String, String[]> parameters = request.getParameterMap();
for (String parameter : parameters.keySet()) {
	if (parameter.equals("reservedTicketId")) {
		for (String parameter2 : parameters.get(parameter)) {
			reservedTicketIds.add(parameter2.trim());
		}
		break;
	}
}

TicketResellerImplService ticketResellerImplService = new TicketResellerImplService();
TicketResellerImpl ticketResellerImpl = ticketResellerImplService.getTicketResellerImplPort();

AbortTransactionRequestType abortTransactionRequest = new AbortTransactionRequestType();
abortTransactionRequest.setUserId(Integer.parseInt(request.getParameter("userId")));
abortTransactionRequest.getReservedTicketIds().addAll(reservedTicketIds);

ticketResellerImpl.abortTransaction(abortTransactionRequest);
%>

<html lang="en">
	<head>
		<title>Ticketing Service</title>
		<!-- Meta data -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Distributed Computing: Ticketing Service">
		<meta name="author" content="Ahmet Can Büyükdemir and Chennara Nhes">
		<!-- CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="css/sticky-footer.min.css" rel="stylesheet" media="screen">
		<link href="css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
		<link href="css/error-classes.min.css" rel="stylesheet" media="screen">
	</head>
	<body>
		<div id="wrap">
			<!-- Begin page content -->
			<div class="container">
				<div class="page-header">
					<h1>Ticketing Service<img src="img/golden-ticket.png" width=50 height=50 align="right"></h1>
				</div>
				Unfortunately, your payment failed.<br/><br/>
<%
Properties prop = new Properties();
prop.load(new FileInputStream("local_config.properties"));
String ipAddress = prop.getProperty("ipAddress");

String homepageURL = "http://" + ipAddress + ":8080/TicketReseller";
%>
				Click <a href="<% out.println(homepageURL); %>">here</a> to return to the homepage.
			</div>
			<!-- Push for sticky footer -->
			<div id="push">
			</div>
		</div>
		<!-- Footer -->
		<div id="footer">
			<div class="container">
				<p class="muted credit">Ticketing Service &copy; 2013 &sdot; Ahmet Can Büyükdemir and Chennara Nhes</p>
			</div>
		</div>
	</body>
</html>
