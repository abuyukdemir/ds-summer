<%@ page import="ds.ticketing.frontend.reseller.*" %>
<%@ page import="java.io.FileInputStream" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Properties" %>

<%
String inputFirstName = request.getParameter("inputFirstName");
String inputLastName = request.getParameter("inputLastName");
String inputEmailAddress = request.getParameter("inputEmailAddress");
String inputProvince = request.getParameter("inputProvince");

UserInfo userInfo = new UserInfo();
userInfo.setFirstName(inputFirstName);
userInfo.setLastName(inputLastName);
userInfo.setEmailAddress(inputEmailAddress);
userInfo.setProvince(inputProvince);

RequestToBuyRequestType requestToBuyRequest = new RequestToBuyRequestType();
requestToBuyRequest.setUserInfo(userInfo);

TicketResellerImplService ticketResellerImplService = new TicketResellerImplService();
TicketResellerImpl ticketResellerImpl = ticketResellerImplService.getTicketResellerImplPort();
RequestToBuyResponseType requestToBuyResponse = ticketResellerImpl.requestToBuy(requestToBuyRequest);

if (requestToBuyResponse.getAck().equals("SUCCESS")) {
%>
<h3>Available Tickets</h3>
<form id="reserveTicketsForm" class="form-horizontal" method="post">
	<table class="table">
		<thead>
			<tr>
				<th>Event Name</th>
				<th>Concert Venue</th>
				<th>Seat Number</th>
				<th>Price</th>
				<th>Currency</th>
				<th>Buy</th>
			</tr>
		</thead>
		<tbody>
<%
	List<TicketInfo> availableTickets = requestToBuyResponse.getAvailableTickets();
	for (TicketInfo ticketInfo : availableTickets) {
%>
			<tr>
				<td><% out.println(ticketInfo.getEventName()); %></td>
				<td><% out.println(ticketInfo.getVenueName()); %></td>
				<td><% out.println(ticketInfo.getSeatNumber()); %></td>
				<td><% out.println(ticketInfo.getPrice()); %></td>
				<td><% out.println(ticketInfo.getCurrency()); %></td>
				<td><label class="checkbox ticketCheckbox"><input type="checkbox" name="inputTicketId<% out.println(ticketInfo.getId()); %>" value="<% out.println(ticketInfo.getId()); %>"></label></td>
			</tr>
<%
	}
%>
		</tbody>
	</table>
	<p id="errorReserveTicketsForm" class="error" align="right"></p>
	<input type="hidden" name="inputFirstName" value="<% out.println(inputFirstName); %>">
	<input type="hidden" name="inputLastName" value="<% out.println(inputLastName); %>">
	<input type="hidden" name="inputEmailAddress" value="<% out.println(inputEmailAddress); %>">
	<input type="hidden" name="inputProvince" value="<% out.println(inputProvince); %>">
	<div class="control-group">
		<div class="controls">
			<button id="reserveTicketsButton" type="submit" class="btn">Submit</button>
		</div>
	</div>
</form>
<script src="js/ticket-reseller/reserve-tickets.js" type="text/javascript"></script>
<%
} else {
%>
<script src="js/ticket-reseller/request-to-buy-fail.js" type="text/javascript"></script>
<%
	Properties prop = new Properties();
	prop.load(new FileInputStream("local_config.properties"));
	
	String ipAddress = prop.getProperty("ipAddress");
	String homepageURL = "http://" + ipAddress + ":8080/TicketReseller";
	
	if (requestToBuyResponse.getAck().equals("MAXIMUM_NUMBER_OF_TICKETS_SOLD")) {
%>
Too bad, we are sold out.<br/>
Hurry up and pack your bags, because we still have some tickets left at the day of the festival!<br/><br/>
<%
	} else if (requestToBuyResponse.getAck().equals("MAXIMUM_NUMBER_OF_TICKETS_TO_PROVINCE_SOLD")) {
%>
Too bad, we have no more tickets left for the <% out.println(inputProvince); %> province.<br/>
Hurry up and pack your bags, because we still have some tickets left at the day of the festival!<br/><br/>
<%
	}
%>
Click <a href="<% out.println(homepageURL); %>">here</a> to return to the homepage.
<%
}
%>
