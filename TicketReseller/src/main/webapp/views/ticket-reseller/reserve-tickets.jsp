<%@ page import="ds.ticketing.frontend.reseller.*" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>

<%
ReserveTicketsRequestType reserveTicketsRequest = new ReserveTicketsRequestType();

String inputFirstName = request.getParameter("inputFirstName");
String inputLastName = request.getParameter("inputLastName");
String inputEmailAddress = request.getParameter("inputEmailAddress");
String inputProvince = request.getParameter("inputProvince");

UserInfo userInfo = new UserInfo();
userInfo.setFirstName(inputFirstName);
userInfo.setLastName(inputLastName);
userInfo.setEmailAddress(inputEmailAddress);
userInfo.setProvince(inputProvince);

reserveTicketsRequest.setUserInfo(userInfo);

Map<String, String[]> parameters = request.getParameterMap();
for (String parameter : parameters.keySet()) {
	if (parameter.startsWith("inputTicketId")) {
		assert(parameters.get(parameter).length == 1);
		reserveTicketsRequest.getReservingTicketIds().add(parameters.get(parameter)[0]);
	}
}

TicketResellerImplService ticketResellerImplService = new TicketResellerImplService();
TicketResellerImpl ticketResellerImpl = ticketResellerImplService.getTicketResellerImplPort();
ReserveTicketsResponseType reserveTicketsResponse = ticketResellerImpl.reserveTickets(reserveTicketsRequest);

if (reserveTicketsResponse.getAck().equals("SUCCESS")) {
%>
<script src="js/ticket-reseller/reserve-tickets-success.js" type="text/javascript"></script>
<form id="makePaymentForm" action="controllers/paypal/make-payment.jsp" method="post">
	<fieldset>
		<input type="hidden" name="inputUserId" value="<% out.println(reserveTicketsResponse.getUserId()); %>">
<%
	for (TicketInfo ticketInfo : reserveTicketsResponse.getReservedTickets()) {
%>
		<input type="hidden" name="inputTicketId<% out.println(ticketInfo.getId()); %>" value="<% out.println(ticketInfo.getId()); %>">
		<input type="hidden" name="inputTicketEventName<% out.println(ticketInfo.getId()); %>" value="<% out.println(ticketInfo.getEventName()); %>">
		<input type="hidden" name="inputTicketVenueName<% out.println(ticketInfo.getId()); %>" value="<% out.println(ticketInfo.getVenueName()); %>">
		<input type="hidden" name="inputTicketSeatNumber<% out.println(ticketInfo.getId()); %>" value="<% out.println(ticketInfo.getSeatNumber()); %>">
		<input type="hidden" name="inputTicketPrice<% out.println(ticketInfo.getId()); %>" value="<% out.println(ticketInfo.getPrice()); %>">
		<input type="hidden" name="inputTicketCurrency<% out.println(ticketInfo.getId()); %>" value="<% out.println(ticketInfo.getCurrency()); %>">
<%
	}
%>
		<h3>Make Payment<input type="image" src="img/btn-express-checkout.gif" align="right"></h3>
		<span class="help-block" align="right">What is <a href="https://www.paypal.com/mx/cgi-%20bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside">PayPal</a>?</span>
	</fieldset>
</form>
<%
} else if (reserveTicketsResponse.getAck().equals("TICKET_ALREADY_RESERVED")) {
%>
<script src="js/ticket-reseller/reserve-tickets-fail.js" type="text/javascript"></script>
<%
// TODO: Make another request for available tickets, automatically removing already reserved tickets
}
%>
