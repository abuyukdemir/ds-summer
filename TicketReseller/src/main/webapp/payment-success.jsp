<%@page contentType="text/html;charset=UTF-8"%>

<%@ page import="ds.ticketing.frontend.paypal.*" %>
<%@ page import="ds.ticketing.frontend.reseller.*" %>
<%@ page import="java.io.FileInputStream" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Properties" %>

<%
GetExpressCheckoutDetailsRequestType getExpressCheckoutDetailsRequest = new GetExpressCheckoutDetailsRequestType();
getExpressCheckoutDetailsRequest.setToken(request.getParameter("token"));
getExpressCheckoutDetailsRequest.setPayerId(request.getParameter("PayerID"));

ExpressCheckoutImplService expressCheckoutImplService = new ExpressCheckoutImplService();
ExpressCheckoutImpl expressCheckoutImpl = expressCheckoutImplService.getExpressCheckoutImplPort();
GetExpressCheckoutDetailsResponseType getExpressCheckoutDetailsResponse = expressCheckoutImpl.getExpressCheckoutDetails(getExpressCheckoutDetailsRequest);

Properties prop = new Properties();
prop.load(new FileInputStream("local_config.properties"));
String ipAddress = prop.getProperty("ipAddress");

String homepageURL = "http://" + ipAddress + ":8080/TicketReseller";
String cancelURL = "http://" + ipAddress + ":8080/TicketReseller/payment-fail.jsp";

String firstName = null;
String lastName = null;
String emailAddress = null;
String province = null;
List<ds.ticketing.frontend.reseller.TicketInfo> reservedTickets = null;

if (getExpressCheckoutDetailsResponse.getAck().equals("SUCCESS")) {
	DoExpressCheckoutPaymentRequestType doExpressCheckoutPaymentRequest = new DoExpressCheckoutPaymentRequestType();
	doExpressCheckoutPaymentRequest.setToken(request.getParameter("token"));
	doExpressCheckoutPaymentRequest.setPayerId(request.getParameter("PayerID"));
	doExpressCheckoutPaymentRequest.getPaymentDetails().addAll(getExpressCheckoutDetailsResponse.getPaymentDetails());
	
	DoExpressCheckoutPaymentResponseType doExpressCheckoutPaymentResponse = expressCheckoutImpl.doExpressCheckoutPayment(doExpressCheckoutPaymentRequest);
	if (doExpressCheckoutPaymentResponse.getAck().equals("SUCCESS")) {
		TicketResellerImplService ticketResellerImplService = new TicketResellerImplService();
		TicketResellerImpl ticketResellerImpl = ticketResellerImplService.getTicketResellerImplPort();
		List<String> reservedTicketIds = new LinkedList<String>();
		Map<String, String[]> parameters = request.getParameterMap();
		for (String parameter : parameters.keySet()) {
			if (parameter.equals("reservedTicketId")) {
				for (String parameter2 : parameters.get(parameter)) {
					reservedTicketIds.add(parameter2.trim());
				}
				break;
			}
		}
		GetReservedTicketsInfoRequestType getReservedTicketsInfoRequest = new GetReservedTicketsInfoRequestType();
		getReservedTicketsInfoRequest.getReservedTicketIds().addAll(reservedTicketIds);
		GetReservedTicketsInfoResponseType getReservedTicketsInfoResponse = ticketResellerImpl.getReservedTicketsInfo(getReservedTicketsInfoRequest);
		firstName = getReservedTicketsInfoResponse.getUserInfo().getFirstName();
		lastName = getReservedTicketsInfoResponse.getUserInfo().getLastName();
		emailAddress = getReservedTicketsInfoResponse.getUserInfo().getEmailAddress();
		province = getReservedTicketsInfoResponse.getUserInfo().getProvince();
		reservedTickets = getReservedTicketsInfoResponse.getReservedTickets();
		
		String currency = "";
		double amount = 0;
		for (ds.ticketing.frontend.reseller.TicketInfo reservedTicket : getReservedTicketsInfoResponse.getReservedTickets()) {			
			amount += reservedTicket.getPrice();
			currency = reservedTicket.getCurrency();
		}
		
		FinalizeTransactionRequestType finalizeTransactionRequest = new FinalizeTransactionRequestType();
		finalizeTransactionRequest.setUserId(Integer.parseInt(request.getParameter("userId")));
		finalizeTransactionRequest.getReservedTicketIds().addAll(reservedTicketIds);
		finalizeTransactionRequest.setAmount(amount);
		finalizeTransactionRequest.setCurrency(currency);
		
		ticketResellerImpl.finalizeTransaction(finalizeTransactionRequest);
	} else {
		response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
		response.setHeader("Location", cancelURL);
	}
} else {
	response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
	response.setHeader("Location", cancelURL);
}
%>

<html lang="en">
	<head>
		<title>Ticketing Service</title>
		<!-- Meta data -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Distributed Computing: Ticketing Service">
		<meta name="author" content="Ahmet Can Büyükdemir and Chennara Nhes">
		<!-- CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="css/sticky-footer.min.css" rel="stylesheet" media="screen">
		<link href="css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
		<link href="css/error-classes.min.css" rel="stylesheet" media="screen">
		<!-- JavaScript -->
		<script src="js/jquery.min.js" type="text/javascript"></script>
	</head>
	<body>
		<div id="wrap">
			<!-- Begin page content -->
			<div class="container">
				<div class="page-header">
					<h1>Ticketing Service<img src="img/golden-ticket.png" width=50 height=50 align="right"></h1>
				</div>
				<!-- Progress bar -->
				<div id="progressBarDiv" class="container">
					<h3>Progress</h3>
					<div class="progress progress-info progress-striped active">
						<div id="progressBar" class="bar" style="width: 100%"></div>
					</div>
				</div>
				<br>
				<!-- Request to buy section -->
				<div id="requestToBuyDiv" class="container">
					<h3>User Information</h3>
					<form id="requestToBuyForm" class="form-horizontal" method="post">
						<div class="control-group">
							<label id="inputFirstNameLabel" class="control-label" for="inputFirstName">First Name</label>
							<div class="controls">
								<span class="uneditable-input"><% out.println(firstName); %></span>
							</div>
						</div>
						<div class="control-group">
							<label id="inputLastNameLabel" class="control-label" for="inputLastName">Last Name</label>
							<div class="controls">
								<span class="uneditable-input"><% out.println(lastName); %></span>
							</div>
						</div>
						<div class="control-group">
							<label id="inputEmailAddressLabel" class="control-label" for="inputEmailAddress">Email Address</label>
							<div class="controls">
								<span class="uneditable-input"><% out.println(emailAddress); %></span>
							</div>
						</div>
						<div class="control-group">
							<label id="inputProvinceLabel" class="control-label" for="inputProvince">Province</label>
							<div class="controls">
								<span class="uneditable-input"><% out.println(province); %></span>
							</div>
						</div>
					</form>
				</div>
				<br>
				<!-- Reserve tickets section -->
				<div id="reserveTicketsDiv" class="container">
					<h3>Reserved Tickets</h3>
					<form id="reserveTicketsForm" class="form-horizontal" method="post">
						<table class="table">
							<thead>
								<tr>
									<th>Event Name</th>
									<th>Concert Venue</th>
									<th>Seat Number</th>
									<th>Price</th>
									<th>Currency</th>
									<th>Buy</th>
								</tr>
							</thead>
							<tbody>
<%
for (ds.ticketing.frontend.reseller.TicketInfo ticketInfo : reservedTickets) {
%>
								<tr>
									<td><% out.println(ticketInfo.getEventName()); %></td>
									<td><% out.println(ticketInfo.getVenueName()); %></td>
									<td><% out.println(ticketInfo.getSeatNumber()); %></td>
									<td><% out.println(ticketInfo.getPrice()); %></td>
									<td><% out.println(ticketInfo.getCurrency()); %></td>
									<td><i class="icon-ok"></i></td>
								</tr>
<%
}
%>
							</tbody>
						</table>
					</form>
				</div>
				<!-- Show notification -->
				<div id="notification">
					<h3>Hello World!</h3>
					Thank you for buying these tickets.<br/>
					It is not too late to buy more tickets!<br/><br/>
					Click <a href="<% out.println(homepageURL); %>">here</a> to return to the homepage.
				</div>
			</div>
			<!-- Push for sticky footer -->
			<div id="push">
			</div>
		</div>
		<!-- Footer -->
		<div id="footer">
			<div class="container">
				<p class="muted credit">Ticketing Service &copy; 2013 &sdot; Ahmet Can Büyükdemir and Chennara Nhes</p>
			</div>
		</div>
	</body>
</html>
