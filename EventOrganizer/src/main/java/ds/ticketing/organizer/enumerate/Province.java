package ds.ticketing.organizer.enumerate;

public enum Province {
	
	ANTWERP("Antwerp"),
	EAST_FLANDERS("East Flanders"),
	FLEMISH_BRABANT("Flemish Brabant"),
	HAINAUT("Hainaut"),
	LIMBURG("Limburg"),
	LIEGE("Liège"),
	LUXEMBOURG("Luxembourg"),
	NAMUR("Namur"),
	WALLOON_BRABANT("Walloon Brabant"),
	WEST_FLANDERS("West Flanders");
	
	private final String name;
	
	private Province(final String name) {
		this.name = name;
	}
	
	public static boolean contains(String name) {
		for (Province province : Province.values()) {
			if (province.name.equals(name)) {
				return true;
			}
		}
		return false;
	}
	
	public String toString() {
		return this.name;
	}
	
}
