package ds.ticketing.organizer.enumerate;

public enum Ack {
	
	SUCCESS("SUCCESS"),
	PROVINCE_UNKNOWN("PROVINCE_UKNOWN"),
	MAXIMUM_NUMBER_OF_TICKETS_SOLD("MAXIMUM_NUMBER_OF_TICKETS_SOLD"),
	MAXIMUM_NUMBER_OF_TICKETS_TO_PROVINCE_SOLD("MAXIMUM_NUMBER_OF_TICKETS_TO_PROVINCE_SOLD"),
	TICKET_ALREADY_RESERVED("TICKET_ALREADY_RESERVED"),
	ERROR("ERROR");
	
	private final String name;
	
	private Ack(String name) {
		this.name = name;
	}
	
	public String toString() {
		return this.name;
	}
	
}
