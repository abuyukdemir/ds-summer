package ds.ticketing.organizer.enumerate;

public enum Currency {
	
	AUD("AUD"), // Australian Dollar
	CAD("CAD"), // Canadian Dollar
	CZK("CZK"), // Czech Koruna
	DKK("DKK"), // Danish Krone
	EUR("EUR"), // Euro
	HKD("HKD"), // Honk Kong Dollar
	HUF("HUF"), // Hungarian Forint
	JPY("JPY"), // Japanese Yen
	NOK("NOK"), // Norwegian Krone
	NZD("NZD"), // New Zealand Dollar
	PLN("PLN"), // Polish Zloty
	GBP("GBP"), // Pound Sterling
	SGD("SGD"), // Singapore Dollar
	SEK("SEK"), // Swedish Krona
	CHF("CHF"), // Swiss Franc
	USD("USD"); // U.S. Dollar
	
	private final String name;
	
	private Currency(String name) {
		this.name = name;
	}
	
	public static boolean contains(String name) {
		for (Currency currency : Currency.values()) {
			if (currency.name().equals(name)) {
				return true;
			}
		}
		return false;
	}
	
	public String toString() {
		return this.name;
	}
	
}
