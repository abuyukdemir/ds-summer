package ds.ticketing.organizer;

import ds.ticketing.organizer.complex.AbortTransactionResponseType;
import ds.ticketing.organizer.complex.AbortTransactionRequestType;
import ds.ticketing.organizer.complex.BootstrapEventOrganizerResponseType;
import ds.ticketing.organizer.complex.FinalizeTransactionRequestType;
import ds.ticketing.organizer.complex.FinalizeTransactionResponseType;
import ds.ticketing.organizer.complex.GetReservedTicketsInfoRequestType;
import ds.ticketing.organizer.complex.GetReservedTicketsInfoResponseType;
import ds.ticketing.organizer.complex.RequestPaymentInformationResponseType;
import ds.ticketing.organizer.complex.RequestToBuyRequestType;
import ds.ticketing.organizer.complex.RequestToBuyResponseType;
import ds.ticketing.organizer.complex.ReserveTicketsRequestType;
import ds.ticketing.organizer.complex.ReserveTicketsResponseType;

public interface EventOrganizer {
	
	public BootstrapEventOrganizerResponseType bootstrapEventOrganizer();
	
	public RequestToBuyResponseType requestToBuy(RequestToBuyRequestType request);
	
	public ReserveTicketsResponseType reserveTickets(ReserveTicketsRequestType request);
	
	public GetReservedTicketsInfoResponseType getReservedTicketsInfo(GetReservedTicketsInfoRequestType request);
	
	public AbortTransactionResponseType abortTransaction(AbortTransactionRequestType request);
	
	public FinalizeTransactionResponseType finalizeTransaction(FinalizeTransactionRequestType request);
	
	public RequestPaymentInformationResponseType requestPaymentInformation();
	
}
