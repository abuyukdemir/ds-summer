package ds.ticketing.organizer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import javax.jws.WebMethod;
import javax.jws.WebService;

import ds.ticketing.bank.BankImpl;
import ds.ticketing.bank.BankImplProxy;
import ds.ticketing.bank.RegisterNewAccountRequestType;
import ds.ticketing.bank.RegisterNewAccountResponseType;
import ds.ticketing.organizer.complex.AbortTransactionRequestType;
import ds.ticketing.organizer.complex.AbortTransactionResponseType;
import ds.ticketing.organizer.complex.BootstrapEventOrganizerResponseType;
import ds.ticketing.organizer.complex.FinalizeTransactionRequestType;
import ds.ticketing.organizer.complex.FinalizeTransactionResponseType;
import ds.ticketing.organizer.complex.GetReservedTicketsInfoRequestType;
import ds.ticketing.organizer.complex.GetReservedTicketsInfoResponseType;
import ds.ticketing.organizer.complex.RequestPaymentInformationResponseType;
import ds.ticketing.organizer.complex.RequestToBuyRequestType;
import ds.ticketing.organizer.complex.RequestToBuyResponseType;
import ds.ticketing.organizer.complex.ReserveTicketsRequestType;
import ds.ticketing.organizer.complex.ReserveTicketsResponseType;
import ds.ticketing.organizer.complex.TicketInfo;
import ds.ticketing.organizer.complex.UserInfo;
import ds.ticketing.organizer.enumerate.Ack;
import ds.ticketing.organizer.enumerate.Province;
import ds.ticketing.venue.ConcertVenueImpl;
import ds.ticketing.venue.ConcertVenueImplProxy;
import ds.ticketing.venue.GetVenueInfoRequestType;
import ds.ticketing.venue.GetVenueInfoResponseType;
import ds.ticketing.venue.VenueInfo;

@WebService
public class EventOrganizerImpl implements EventOrganizer {
	
	private static final Logger LOGGER = Logger.getLogger(EventOrganizerImpl.class.getPackage().getName());
	
	private Map<String, Integer> maxNumberOfTicketsPerProvince = new HashMap<String, Integer>();
	private int holdNumberOfTickets;
	
	private String connectionURL;
	private String databaseUsername;
	private String databasePassword;
	
	public EventOrganizerImpl() {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		try {
			Properties prop = new Properties();
			
			prop.load(new FileInputStream("local_config.properties"));
			String hostname = prop.getProperty("hostname");
			
			this.connectionURL = "jdbc:derby:/home/" + hostname + "/eclipse/workspace/EventOrganizerDatabase";
			prop.load(new FileInputStream("organizer_config.properties"));
			this.maxNumberOfTicketsPerProvince.put(Province.ANTWERP.toString(), Integer.parseInt(prop.getProperty("maxNumberOfTickets.Antwerp")));
			this.maxNumberOfTicketsPerProvince.put(Province.EAST_FLANDERS.toString(), Integer.parseInt(prop.getProperty("maxNumberOfTickets.EastFlanders")));
			this.maxNumberOfTicketsPerProvince.put(Province.FLEMISH_BRABANT.toString(), Integer.parseInt(prop.getProperty("maxNumberOfTickets.FlemishBrabant")));
			this.maxNumberOfTicketsPerProvince.put(Province.HAINAUT.toString(), Integer.parseInt(prop.getProperty("maxNumberOfTickets.Hainaut")));
			this.maxNumberOfTicketsPerProvince.put(Province.LIMBURG.toString(), Integer.parseInt(prop.getProperty("maxNumberOfTickets.Limburg")));
			this.maxNumberOfTicketsPerProvince.put(Province.LIEGE.toString(), Integer.parseInt(prop.getProperty("maxNumberOfTickets.Liege")));
			this.maxNumberOfTicketsPerProvince.put(Province.LUXEMBOURG.toString(), Integer.parseInt(prop.getProperty("maxNumberOfTickets.Luxembourg")));
			this.maxNumberOfTicketsPerProvince.put(Province.NAMUR.toString(), Integer.parseInt(prop.getProperty("maxNumberOfTickets.Namur")));
			this.maxNumberOfTicketsPerProvince.put(Province.WALLOON_BRABANT.toString(), Integer.parseInt(prop.getProperty("maxNumberOfTickets.WalloonBrabant")));
			this.maxNumberOfTicketsPerProvince.put(Province.WEST_FLANDERS.toString(), Integer.parseInt(prop.getProperty("maxNumberOfTickets.WestFlanders")));
			this.holdNumberOfTickets = Integer.parseInt(prop.getProperty("holdNumberOfTickets"));
			
			this.databaseUsername = prop.getProperty("database.Username");
			this.databasePassword = prop.getProperty("database.Password");
		} catch (IOException e) {
			LOGGER.severe(e.getMessage());
		}
	}
	
	@Override
	@WebMethod
	public BootstrapEventOrganizerResponseType bootstrapEventOrganizer() {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		try {
			return this.bootstrapEventOrganizerWrapped();
		} catch (SQLException e) {
			LOGGER.severe(e.getMessage());
			return new BootstrapEventOrganizerResponseType().setAck(Ack.ERROR.toString());
		}
	}
	
	private BootstrapEventOrganizerResponseType bootstrapEventOrganizerWrapped() throws SQLException {
		BootstrapEventOrganizerResponseType response = new BootstrapEventOrganizerResponseType();
		
		Connection connection = null;
		Statement insertVenueStatement = null;
		Statement insertEventStatement = null;
		PreparedStatement insertTicketsStatement = null;
		ResultSet resultSet = null;
		
		try {
			// Read event organizer information from properties file
			Properties prop = new Properties();
			prop.load(new FileInputStream("organizer_config.properties"));
			String eventName = prop.getProperty("eventName");
			String address = prop.getProperty("address");
			String province = prop.getProperty("province");
			String postalCode = prop.getProperty("postalCode");
			double startCapital = Double.parseDouble(prop.getProperty("bankAccount.StartCapital"));
			String currency = prop.getProperty("bankAccount.Currency");
			double ticketPrice = Double.parseDouble(prop.getProperty("ticketInfo.Price"));
			
			
			// Request detailed concert venue information
			GetVenueInfoRequestType getVenueInfoRequest = new GetVenueInfoRequestType();
			getVenueInfoRequest.setEventName(eventName);
			
			ConcertVenueImplProxy concertVenueImplProxy = new ConcertVenueImplProxy();
			ConcertVenueImpl concertVenueImpl = concertVenueImplProxy.getConcertVenueImpl();
			GetVenueInfoResponseType getVenueInfoResponse = concertVenueImpl.getVenueInfo(getVenueInfoRequest);
			
			
			if (getVenueInfoResponse.getAck().equals(Ack.SUCCESS.toString())) {
				// Set up a Derby database connection and set auto commit to false, enabling transactions
				connection = DriverManager.getConnection(this.connectionURL, this.databaseUsername, this.databasePassword);
				connection.setAutoCommit(false);
				connection.setTransactionIsolation(2); // connection.setTransactionIsolation(TRANSACTION_READ_COMMITTED);
				
				
				// Extract concert venue information from returned response
				VenueInfo venueInfo = getVenueInfoResponse.getVenueInfo();
				String venueName = venueInfo.getVenueName();
				assert(venueInfo.getEventName().equals(eventName));
				int capacity = venueInfo.getCapacity();
				int seated = venueInfo.getSeated();
				int numberOfRows = venueInfo.getNumberOfRows();
				int numberOfColumns = venueInfo.getNumberOfColumns();
				
				
				// Add concert venue information to database
				String insertVenueQuery = "INSERT INTO venues(venueName, capacity, seated, numberOfRows, numberOfColumns) " +
				                          "VALUES('" + venueName + "', " + capacity + ", " + seated + ", " + numberOfRows + ", " + numberOfColumns + ")";
				insertVenueStatement = connection.createStatement();
				int numberOfRowsAffected = insertVenueStatement.executeUpdate(insertVenueQuery);
				assert(numberOfRowsAffected == 1);
				
				
				// Add event organizer information to events table in the database
				String insertEventQuery = "INSERT INTO events(eventName, venueName) " + "VALUES('" + eventName + "', '" + venueName + "')";
				insertEventStatement = connection.createStatement();
				numberOfRowsAffected = insertEventStatement.executeUpdate(insertEventQuery);
				assert(numberOfRowsAffected == 1);
				
				
				// Generated tickets and add them to the database
				String insertTicketsQuery = "INSERT INTO tickets(eventName, venueName, rowNumber, columnNumber, price, currency, reserved, bought) " +
				                            "VALUES('" + eventName + "', '" + venueName + "', ?, ?, ?" + ", '" + currency + "', 0, 0)";
				insertTicketsStatement = connection.prepareStatement(insertTicketsQuery);
				
				for (int i = 1; i <= numberOfRows; ++i) {
					for (int j = 1; j <= numberOfColumns; ++j) {
						insertTicketsStatement.setInt(1, i);
						insertTicketsStatement.setInt(2, j);
						insertTicketsStatement.setDouble(3, ticketPrice);
						insertTicketsStatement.addBatch();
					}
				}
				
				insertTicketsStatement.executeBatch();
				
				
				// Don't forget to commit the transaction!
				connection.commit();
				
				
				// Register a bank account
				RegisterNewAccountRequestType registerNewAccountRequest = new RegisterNewAccountRequestType();
				registerNewAccountRequest.setFirstName(eventName);
				registerNewAccountRequest.setLastName("Event");
				registerNewAccountRequest.setAddress(address);
				registerNewAccountRequest.setProvince(province);
				registerNewAccountRequest.setPostalCode(postalCode);
				registerNewAccountRequest.setStartCapital(startCapital);
				registerNewAccountRequest.setCurrency(currency);
				
				BankImplProxy bankImplProxy = new BankImplProxy();
				BankImpl bankImpl = bankImplProxy.getBankImpl();
				
				RegisterNewAccountResponseType registerNewAccountResponse = bankImpl.registerNewAccount(registerNewAccountRequest);
				
				if (registerNewAccountResponse.getAck().equals(Ack.SUCCESS.toString())) {
					prop.setProperty("bankAccount.Id", Integer.toString(registerNewAccountResponse.getAccountId()));
					prop.store(new FileOutputStream("organizer_config.properties"), null);
					
					response.setAck(Ack.SUCCESS.toString());
				} else {
					response.setAck(Ack.ERROR.toString());
				}
			} else {
				response.setAck(Ack.ERROR.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			response.setAck(Ack.ERROR.toString());
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}
			if (insertTicketsStatement != null) {
				insertTicketsStatement.close();
			}
			if (insertEventStatement != null) {
				insertEventStatement.close();
			}
			if (insertVenueStatement != null) {
				insertVenueStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
		
		return response;
	}
	
	@Override
	@WebMethod
	public RequestToBuyResponseType requestToBuy(RequestToBuyRequestType request) {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		try {
			request = request.trim();
			return this.requestToBuyWrapped(request);
		} catch (SQLException e) {
			LOGGER.severe(e.getMessage());
			return new RequestToBuyResponseType().setAck(Ack.ERROR.toString());
		}
	}
	
	private RequestToBuyResponseType requestToBuyWrapped(RequestToBuyRequestType request) throws SQLException {
		RequestToBuyResponseType response = new RequestToBuyResponseType();
		
		Connection connection = null;
		Statement selectTotalNumberOfTicketsStatement = null;
		Statement selectNumberOfAvailableTicketsForProvinceStatement = null;
		Statement selectAvailableTicketsStatement = null;
		ResultSet resultSet = null;
		
		try {
			// Set up a Derby database connection and set auto commit to false, enabling transactions
			connection = DriverManager.getConnection(this.connectionURL, this.databaseUsername, this.databasePassword);
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(2); // connection.setTransactionIsolation(TRANSACTION_READ_COMMITTED);
			
			
			// Select total number of tickets from database
			List<TicketInfo> availableTickets = new LinkedList<TicketInfo>();
			
			selectTotalNumberOfTicketsStatement = connection.createStatement();
			String selectTotalNumberOfTicketsQuery = "SELECT COUNT(*) AS count FROM tickets";
			resultSet = selectTotalNumberOfTicketsStatement.executeQuery(selectTotalNumberOfTicketsQuery);
			resultSet.next();
			
			int totalNumberOfTickets = resultSet.getInt("count");
			
			
			// Select number of already sold tickets to inhabitans of this province
			selectNumberOfAvailableTicketsForProvinceStatement = connection.createStatement();
			String selectNumberOfAvailableTicketsForProvinceQuery = "SELECT COUNT(*) AS count FROM tickets WHERE EXISTS ( " +
			                                                        "SELECT userId FROM users WHERE " +
			                                                        "users.userId = tickets.userId and users.province = '" + request.userInfo.province + "')";
			resultSet = selectNumberOfAvailableTicketsForProvinceStatement.executeQuery(selectNumberOfAvailableTicketsForProvinceQuery);
			resultSet.next();
			
			int numberOfSoldTicketsForProvince = resultSet.getInt("count");
			
			if (Province.contains(request.userInfo.province)) {
				if (numberOfSoldTicketsForProvince < this.maxNumberOfTicketsPerProvince.get(request.userInfo.province)) {
					// Select available tickets from database
					selectAvailableTicketsStatement = connection.createStatement();
					String selectAvailableTicketsQuery = "SELECT ticketId, eventName, venueName, rowNumber, columnNumber, price, currency FROM tickets WHERE " +
					                                     "reserved = 0 and bought = 0";
					resultSet = selectAvailableTicketsStatement.executeQuery(selectAvailableTicketsQuery);
					
					int numberOfAvailableTickets = totalNumberOfTickets - this.holdNumberOfTickets;
					
					if (numberOfAvailableTickets >= 0) { 
						while (resultSet.next()) {
							int ticketId = resultSet.getInt("ticketId");
							String eventName = resultSet.getString("eventName");
							String venueName = resultSet.getString("venueName");
							int rowNumber = resultSet.getInt("rowNumber");
							int columnNumber = resultSet.getInt("columnNumber");
							double price = resultSet.getDouble("price");
							String currency = resultSet.getString("currency");
							
							String seatNumber = rowNumber + "/" + columnNumber;
							
							TicketInfo ticketInfo = new TicketInfo().setId(ticketId).setEventName(eventName).setVenueName(venueName).
							                                         setSeatNumber(seatNumber).setPrice(price).setCurrency(currency);
							availableTickets.add(ticketInfo);
							
							response.setAck(Ack.SUCCESS.toString());
						}
					} else {
						response.setAck(Ack.MAXIMUM_NUMBER_OF_TICKETS_SOLD.toString());
					}
				} else {
					response.setAck(Ack.MAXIMUM_NUMBER_OF_TICKETS_TO_PROVINCE_SOLD.toString());
				}
			} else {
				response.setAck(Ack.PROVINCE_UNKNOWN.toString());
			}
			
			
			// Don't forget to commit!
			connection.commit();
			
			response.setUserInfo(request.userInfo).setAvailableTickets(availableTickets.toArray(new TicketInfo[availableTickets.size()]));
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
			connection.rollback();
			response = new RequestToBuyResponseType().setAck(Ack.ERROR.toString());
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}
			if (selectAvailableTicketsStatement != null) {
				selectAvailableTicketsStatement.close();
			}
			if (selectTotalNumberOfTicketsStatement != null) {
				 selectTotalNumberOfTicketsStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
		
		return response;
	}
	
	@Override
	@WebMethod
	public ReserveTicketsResponseType reserveTickets(ReserveTicketsRequestType request) {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		try {
			request = request.trim();
			return this.reserveTicketsWrapped(request);
		} catch (SQLException e) {
			LOGGER.severe(e.getMessage());
			return new ReserveTicketsResponseType().setAck(Ack.ERROR.toString());
		}
	}
	
	private ReserveTicketsResponseType reserveTicketsWrapped(ReserveTicketsRequestType request) throws SQLException {
		ReserveTicketsResponseType response = new ReserveTicketsResponseType();
		
		Connection connection = null;
		Statement selectAvailableTicketsStatement = null;
		Statement selectUserStatement = null;
		Statement insertUserStatement = null;
		Statement selectUserStatement2 = null;
		Statement updateReservingTicketsStatement = null;
		ResultSet resultSet = null;
		
		try {
			// Set up a Derby database connection and set auto commit to false, enabling transactions
			connection = DriverManager.getConnection(this.connectionURL, this.databaseUsername, this.databasePassword);
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(2); // connection.setTransactionIsolation(TRANSACTION_READ_COMMITTED);
			
			
			// Select available tickets from database
			List<TicketInfo> reservingTickets = new LinkedList<TicketInfo>();
			
			String reservingTicketsQuery = "";
			for (int i = 0; i < request.reservingTicketIds.length; ++i) {
				if (i != request.reservingTicketIds.length - 1) {
					reservingTicketsQuery += "ticketId = " + request.reservingTicketIds[i] + " or ";
				} else {
					reservingTicketsQuery += "ticketId = " + request.reservingTicketIds[i];
				}
			}
			
			selectAvailableTicketsStatement = connection.createStatement();
			String selectAvailableTicketsQuery = "SELECT ticketId, eventName, venueName, rowNumber, columnNumber, " +
			                                     "price, currency, reserved FROM tickets WHERE " + reservingTicketsQuery;
			resultSet = selectAvailableTicketsStatement.executeQuery(selectAvailableTicketsQuery);
			
			while (resultSet.next()) {
				int ticketId = resultSet.getInt("ticketId");
				String eventName = resultSet.getString("eventName");
				String venueName = resultSet.getString("venueName");
				int rowNumber = resultSet.getInt("rowNumber");
				int columnNumber = resultSet.getInt("columnNumber");
				double price = resultSet.getDouble("price");
				String currency = resultSet.getString("currency");
				int reserved = resultSet.getInt("reserved");
				
				String seatNumber = rowNumber + "/" + columnNumber;
				
				if (reserved == 0) {
					response.setAck(Ack.SUCCESS.toString());
					
					TicketInfo ticketInfo = new TicketInfo().setId(ticketId).setEventName(eventName).setVenueName(venueName).
					                                         setSeatNumber(seatNumber).setPrice(price).setCurrency(currency);
					reservingTickets.add(ticketInfo);
				} else if (reserved == 1) {
					response.setAck(Ack.TICKET_ALREADY_RESERVED.toString());
					break;
				}
			}
			
			if (response.ack.equals(Ack.SUCCESS.toString())) {
				// Check if user already exists in the database
				selectUserStatement = connection.createStatement();
				String selectUserQuery = "SELECT userId FROM users WHERE emailAddress = '" + request.userInfo.emailAddress + "'";
				resultSet = selectUserStatement.executeQuery(selectUserQuery);
				
				int userId = -1;
				
				if (!resultSet.next()) {
					// Add user to database
					insertUserStatement = connection.createStatement();
					String insertUserQuery = "INSERT INTO users(firstName, lastName, emailAddress, province) VALUES('" +
					                         request.userInfo.firstName + "', '" + request.userInfo.lastName + "', '" +
					                        request.userInfo.emailAddress + "', '" + request.userInfo.province + "')";
					int numberOfRowsAffected = insertUserStatement.executeUpdate(insertUserQuery);
					assert(numberOfRowsAffected == 1);
					
					
					// Select user's id from database
					selectUserStatement2 = connection.createStatement();
					String selectUserQuery2 = "SELECT userId FROM users WHERE emailAddress = '" + request.userInfo.emailAddress + "'";
					resultSet = selectUserStatement.executeQuery(selectUserQuery2);
					resultSet.next();
				}
				
				userId = resultSet.getInt("userId");
				assert(userId != -1);
				
				
				// Update reserved tickets in database
				updateReservingTicketsStatement = connection.createStatement();
				String updateReservingTicketsQuery = "UPDATE tickets SET userId = " + userId + ", reserved = 1 WHERE " + reservingTicketsQuery;
				int numberOfRowsAffected = updateReservingTicketsStatement.executeUpdate(updateReservingTicketsQuery);
				assert(numberOfRowsAffected == reservingTickets.size());
				
				response.setUserId(userId).setReservedTickets(reservingTickets.toArray(new TicketInfo[reservingTickets.size()]));
			}
			
			
			// Don't forget to commit the transaction!
			connection.commit();
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
			connection.rollback();
			response.setAck(Ack.ERROR.toString());
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}
			if (updateReservingTicketsStatement != null) {
				updateReservingTicketsStatement.close();
			}
			if (selectUserStatement2 != null) {
				selectUserStatement2.close();
			}
			if (insertUserStatement != null) {
				insertUserStatement.close();
			}
			if (selectUserStatement != null) {
				selectUserStatement.close();
			}
			if (selectAvailableTicketsStatement != null) {
				selectAvailableTicketsStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
		
		return response;
	}
	
	@Override
	@WebMethod
	public GetReservedTicketsInfoResponseType getReservedTicketsInfo(GetReservedTicketsInfoRequestType request) {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		try {
			request = request.trim();
			return this.getReservedTicketsInfoWrapped(request);
		} catch (SQLException e) {
			LOGGER.severe(e.getMessage());
			return new GetReservedTicketsInfoResponseType().setAck(Ack.ERROR.toString());
		}
	}
	
	private GetReservedTicketsInfoResponseType getReservedTicketsInfoWrapped(GetReservedTicketsInfoRequestType request) throws SQLException {
		GetReservedTicketsInfoResponseType response = new GetReservedTicketsInfoResponseType();
		
		Connection connection = null;
		Statement selectUserInfoStatement = null;
		Statement selectReservedTicketsStatement = null;
		ResultSet resultSet = null;
		
		try {
			// Set up a Derby database connection
			connection = DriverManager.getConnection(this.connectionURL, this.databaseUsername, this.databasePassword);
			
			
			// Select the tickets from the database
			List<TicketInfo> reservedTickets = new LinkedList<TicketInfo>();
			
			String reservingTicketsQuery = "";
			for (int i = 0; i < request.reservedTicketIds.length; ++i) {
				if (i != request.reservedTicketIds.length - 1) {
					reservingTicketsQuery += "ticketId = " + request.reservedTicketIds[i] + " or ";
				} else {
					reservingTicketsQuery += "ticketId = " + request.reservedTicketIds[i];
				}
			}
			
			selectReservedTicketsStatement = connection.createStatement();
			String selectReservedTicketsQuery = "SELECT ticketId, userId, eventName, venueName, rowNumber, columnNumber, price, currency FROM tickets WHERE " + reservingTicketsQuery;
			resultSet = selectReservedTicketsStatement.executeQuery(selectReservedTicketsQuery);
			
			int userId = -1;
			
			while (resultSet.next()) {
				int ticketId = resultSet.getInt("ticketId");
				userId = resultSet.getInt("userId");
				String eventName = resultSet.getString("eventName");
				String venueName = resultSet.getString("venueName");
				int rowNumber = resultSet.getInt("rowNumber");
				int columnNumber = resultSet.getInt("columnNumber");
				double price = resultSet.getDouble("price");
				String currency = resultSet.getString("currency");
				
				String seatNumber = rowNumber + "/" + columnNumber;
				
				TicketInfo ticketInfo = new TicketInfo().setId(ticketId).setEventName(eventName).setVenueName(venueName).
				                                         setSeatNumber(seatNumber).setPrice(price).setCurrency(currency);
				reservedTickets.add(ticketInfo);
			}
			
			assert(userId != -1);
			
			
			// Select user information from the databse
			selectUserInfoStatement = connection.createStatement();
			String selectUserInfoQuery = "SELECT firstName, lastName, emailAddress, province FROM users WHERE userId = " + userId;
			resultSet = selectReservedTicketsStatement.executeQuery(selectUserInfoQuery);
			resultSet.next();
			String firstName = resultSet.getString("firstName");
			String lastName = resultSet.getString("lastName");
			String emailAddress = resultSet.getString("emailAddress");
			String province = resultSet.getString("province");
			
			UserInfo userInfo = new UserInfo().setFirstName(firstName).setLastName(lastName).setEmailAddress(emailAddress).setProvince(province);
			
			response.setAck(Ack.SUCCESS.toString()).setUserInfo(userInfo).setReservedTickets(reservedTickets.toArray(new TicketInfo[reservedTickets.size()]));
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
			connection.rollback();
			response.setAck(Ack.ERROR.toString());
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}
			if (selectReservedTicketsStatement != null) {
				selectReservedTicketsStatement.close();
			}
			if (selectUserInfoStatement != null) {
				selectUserInfoStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
		
		return response;
	}
	
	@Override
	@WebMethod
	public AbortTransactionResponseType abortTransaction(AbortTransactionRequestType request) {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		try {
			request = request.trim();
			return this.abortTransactionWrapped(request);
		} catch (SQLException e) {
			LOGGER.severe(e.getMessage());
			return new AbortTransactionResponseType().setAck(Ack.ERROR.toString());
		}
	}
	
	private AbortTransactionResponseType abortTransactionWrapped(AbortTransactionRequestType request) throws SQLException {
		AbortTransactionResponseType response = new AbortTransactionResponseType();
		
		Connection connection = null;
		Statement updateTicketStatement = null;
		
		try {
			// Set up a Derby database connection
			connection = DriverManager.getConnection(this.connectionURL, this.databaseUsername, this.databasePassword);
			
			String reservingTicketsQuery = "";
			for (int i = 0; i < request.reservedTicketIds.length; ++i) {
				if (i != request.reservedTicketIds.length - 1) {
					reservingTicketsQuery += "ticketId = " + request.reservedTicketIds[i] + " or ";
				} else {
					reservingTicketsQuery += "ticketId = " + request.reservedTicketIds[i];
				}
			}
			
			updateTicketStatement = connection.createStatement();
			String insertTicketQuery = "UPDATE tickets SET reserved = 0 WHERE " + reservingTicketsQuery + " and userId = " + request.userId;
			updateTicketStatement.executeUpdate(insertTicketQuery);
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
			connection.rollback();
			response.setAck(Ack.ERROR.toString());
		} finally {
			if (updateTicketStatement != null) {
				updateTicketStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
		
		return response;
	}
	
	@Override
	@WebMethod
	public FinalizeTransactionResponseType finalizeTransaction(FinalizeTransactionRequestType request) {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		try {
			request = request.trim();
			return this.finalizeTransactionWrapped(request);
		} catch (SQLException e) {
			LOGGER.severe(e.getMessage());
			return new FinalizeTransactionResponseType().setAck(Ack.ERROR.toString());
		}
	}
	
	private FinalizeTransactionResponseType finalizeTransactionWrapped(FinalizeTransactionRequestType request) throws SQLException {
		FinalizeTransactionResponseType response = new FinalizeTransactionResponseType();
		
		Connection connection = null;
		Statement updateTicketStatement = null;
		
		try {
			// Set up a Derby database connection
			connection = DriverManager.getConnection(this.connectionURL, this.databaseUsername, this.databasePassword);
			
			
			// Update reserved tickets
			String reservingTicketsQuery = "";
			for (int i = 0; i < request.reservedTicketIds.length; ++i) {
				if (i != request.reservedTicketIds.length - 1) {
					reservingTicketsQuery += "ticketId = " + request.reservedTicketIds[i] + " or ";
				} else {
					reservingTicketsQuery += "ticketId = " + request.reservedTicketIds[i];
				}
			}
			
			updateTicketStatement = connection.createStatement();
			String insertTicketQuery = "UPDATE tickets SET reserved = 0, bought = 1 WHERE " + reservingTicketsQuery + " and userId = " + request.userId;
			updateTicketStatement.executeUpdate(insertTicketQuery);
			
			response.setAck(Ack.SUCCESS.toString());
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
			connection.rollback();
			response.setAck(Ack.ERROR.toString());
		} finally {
			if (updateTicketStatement != null) {
				updateTicketStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
			
		return response;
	}
	
	@Override
	@WebMethod
	public RequestPaymentInformationResponseType requestPaymentInformation() {
		RequestPaymentInformationResponseType response = new RequestPaymentInformationResponseType();
		
		try {
			Properties prop = new Properties();
			prop.load(new FileInputStream("organizer_config.properties"));
			int accountId = Integer.parseInt(prop.getProperty("bankAccount.Id"));
			double resellerFee = Double.parseDouble(prop.getProperty("reseller.Fee"));
			response.setAck(Ack.SUCCESS.toString());
			response.setAccountId(accountId);
			response.setResellerFee(resellerFee);
		} catch (IOException e) {
			LOGGER.severe(e.getMessage());
			response.setAck(Ack.ERROR.toString());
		}
		
		return response;
	}
	
}
