package ds.ticketing.organizer.complex;

public class FinalizeTransactionRequestType {
	
	public int userId;
	
	public String[] reservedTicketIds;
	
	public FinalizeTransactionRequestType trim() {
		for (int i = 0; i < this.reservedTicketIds.length; ++i) {
			this.reservedTicketIds[i] = this.reservedTicketIds[i].trim();
		}
		return this;
	}
	
}
