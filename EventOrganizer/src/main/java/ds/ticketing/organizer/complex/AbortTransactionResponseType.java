package ds.ticketing.organizer.complex;

public class AbortTransactionResponseType {
	
	public String ack;
	
	public AbortTransactionResponseType setAck(String ack) {
		this.ack = ack;
		return this;
	}
	
}
