package ds.ticketing.organizer.complex;

public class RequestToBuyResponseType {
	
	public String ack;
	
	public UserInfo userInfo;
	
	public TicketInfo[] availableTickets;
	
	public RequestToBuyResponseType setAck(String ack) {
		this.ack = ack;
		return this;
	}
	
	public RequestToBuyResponseType setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
		return this;
	}
	
	public RequestToBuyResponseType setAvailableTickets(TicketInfo[] availableTickets) {
		this.availableTickets = availableTickets;
		return this;
	}
	
}
