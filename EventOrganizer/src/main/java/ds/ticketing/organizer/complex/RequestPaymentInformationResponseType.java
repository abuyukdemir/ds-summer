package ds.ticketing.organizer.complex;

public class RequestPaymentInformationResponseType {
	
	public String ack;
	
	public int accountId;
	
	public double resellerFee;
	
	public RequestPaymentInformationResponseType setAck(String ack) {
		this.ack = ack;
		return this;
	}
	
	public RequestPaymentInformationResponseType setAccountId(int accountId) {
		this.accountId = accountId;
		return this;
	}
	
	public RequestPaymentInformationResponseType setResellerFee(double resellerFee) {
		this.resellerFee = resellerFee;
		return this;
	}
	
}
