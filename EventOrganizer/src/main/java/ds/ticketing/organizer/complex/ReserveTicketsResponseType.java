package ds.ticketing.organizer.complex;

public class ReserveTicketsResponseType {
	
	public String ack;
	
	public int userId;
	
	public TicketInfo[] reservedTickets;
	
	public ReserveTicketsResponseType setAck(String ack) {
		this.ack = ack;
		return this;
	}
	
	public ReserveTicketsResponseType setUserId(int userId) {
		this.userId = userId;
		return this;
	}
	
	public ReserveTicketsResponseType setReservedTickets(TicketInfo[] reservedTickets) {
		this.reservedTickets = reservedTickets;
		return this;
	}
	
}
