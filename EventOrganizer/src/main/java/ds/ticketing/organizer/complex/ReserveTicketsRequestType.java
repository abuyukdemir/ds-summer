package ds.ticketing.organizer.complex;

public class ReserveTicketsRequestType {
	
	public UserInfo userInfo;
	
	public String[] reservingTicketIds;
	
	public ReserveTicketsRequestType trim() {
		this.userInfo = this.userInfo.trim();
		for (int i = 0; i < this.reservingTicketIds.length; ++i) {
			this.reservingTicketIds[i] = this.reservingTicketIds[i].trim();
		}
		return this;
	}
	
}
