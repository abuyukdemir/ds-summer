package ds.ticketing.organizer.complex;

public class RequestDepositOfCollectedMoneyResponseType {
	
	public String ack;
	
	public RequestDepositOfCollectedMoneyResponseType setAck(String ack) {
		this.ack = ack;
		return this;
	}
	
}
