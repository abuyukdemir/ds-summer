package ds.ticketing.organizer.complex;

public class AbortTransactionRequestType {
	
	public int userId;
	
	public String[] reservedTicketIds;
	
	public AbortTransactionRequestType trim() {
		for (int i = 0; i < this.reservedTicketIds.length; ++i) {
			this.reservedTicketIds[i] = this.reservedTicketIds[i].trim();
		}
		return this;
	}
	
}
