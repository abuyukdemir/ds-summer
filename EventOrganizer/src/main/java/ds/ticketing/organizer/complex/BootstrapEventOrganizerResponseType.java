package ds.ticketing.organizer.complex;

public class BootstrapEventOrganizerResponseType {
	
	public String ack;
	
	public BootstrapEventOrganizerResponseType setAck(String ack) {
		this.ack = ack;
		return this;
	}
	
}
