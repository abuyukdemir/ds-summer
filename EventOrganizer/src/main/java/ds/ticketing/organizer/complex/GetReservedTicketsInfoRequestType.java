package ds.ticketing.organizer.complex;

public class GetReservedTicketsInfoRequestType {
	
	public int dummy;

	public String[] reservedTicketIds;
	
	public GetReservedTicketsInfoRequestType trim() {
		for (int i = 0; i < this.reservedTicketIds.length; ++i) {
			this.reservedTicketIds[i] = this.reservedTicketIds[i].trim();
		}
		return this;
	}
	
}
