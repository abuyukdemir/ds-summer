package ds.ticketing.organizer.complex;

public class UserInfo {
	
	public int id;
	
	public String firstName;
	
	public String lastName;
	
	public String emailAddress;
	
	public String province;
	
	public UserInfo setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}
	
	public UserInfo setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}
	
	public UserInfo setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
		return this;
	}
	
	public UserInfo setProvince(String province) {
		this.province = province;
		return this;
	}
	
	public UserInfo trim() {
		this.firstName = this.firstName.trim();
		this.lastName = this.lastName.trim();
		this.emailAddress = this.emailAddress.trim();
		this.province = this.province.trim();
		return this;
	}
	
}
