package ds.ticketing.organizer.complex;

public class RequestToBuyRequestType {
	
	public UserInfo userInfo;
	
	public RequestToBuyRequestType trim() {
		this.userInfo = this.userInfo.trim();
		return this;
	}
	
}
