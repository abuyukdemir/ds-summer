package ds.ticketing.organizer.complex;

public class GetReservedTicketsInfoResponseType {
	
	public String ack;
	
	public UserInfo userInfo;
	
	public TicketInfo[] reservedTickets;
	
	public GetReservedTicketsInfoResponseType setAck(String ack) {
		this.ack = ack;
		return this;
	}
	
	public GetReservedTicketsInfoResponseType setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
		return this;
	}
	
	public GetReservedTicketsInfoResponseType setReservedTickets(TicketInfo[] reservedTickets) {
		this.reservedTickets = reservedTickets;
		return this;
	}
	
}
