package ds.ticketing.venue.complex;

public class GetVenueInfoResponseType {
	
	public String ack;
	
	public VenueInfo venueInfo;
	
	public GetVenueInfoResponseType setAck(String ack) {
		this.ack = ack;
		return this;
	}
	
	public GetVenueInfoResponseType setVenueInfo(VenueInfo venueInfo) {
		this.venueInfo = venueInfo;
		return this;
	}
	
}
