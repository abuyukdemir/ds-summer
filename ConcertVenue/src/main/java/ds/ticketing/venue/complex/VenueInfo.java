package ds.ticketing.venue.complex;

public class VenueInfo {
	
	public String venueName;
	
	public String eventName;
	
	public int capacity;
	
	public int seated;
	
	public int numberOfRows;
	
	public int numberOfColumns;
	
	public VenueInfo setVenueName(String venueName) {
		this.venueName = venueName;
		return this;
	}
	
	public VenueInfo setEventName(String eventName) {
		this.eventName = eventName;
		return this;
	}
	
	public VenueInfo setCapacity(int capacity) {
		this.capacity = capacity;
		return this;
	}
	
	public VenueInfo setSeated(int seated) {
		this.seated = seated;
		return this;
	}
	
	public VenueInfo setNumberOfRows(int numberOfRows) {
		this.numberOfRows = numberOfRows;
		return this;
	}
	
	public VenueInfo setNumberOfColumns(int numberOfColumns) {
		this.numberOfColumns = numberOfColumns;
		return this;
	}
	
}
