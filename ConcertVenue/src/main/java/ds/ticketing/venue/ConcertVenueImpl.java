package ds.ticketing.venue;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;
import java.util.logging.Logger;

import javax.jws.WebService;

import ds.ticketing.venue.complex.GetVenueInfoRequestType;
import ds.ticketing.venue.complex.GetVenueInfoResponseType;
import ds.ticketing.venue.complex.VenueInfo;
import ds.ticketing.venue.enumerate.Ack;

@WebService
public class ConcertVenueImpl implements ConcertVenue {
	
	private static final Logger LOGGER = Logger.getLogger(ConcertVenueImpl.class.getPackage().getName());
	
	public GetVenueInfoResponseType getVenueInfo(GetVenueInfoRequestType request) {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		GetVenueInfoResponseType response = new GetVenueInfoResponseType();
		
		try {
			Properties prop = new Properties();
			prop.load(new FileInputStream("venue_config.properties"));
			String venueName = prop.getProperty("venueName");
			String eventName = prop.getProperty("eventName");
			int capacity = Integer.parseInt(prop.getProperty("capacity"));
			int seated = Integer.parseInt(prop.getProperty("seated"));
			int numberOfRows = Integer.parseInt(prop.getProperty("numberOfRows"));
			int numberOfColumns = Integer.parseInt(prop.getProperty("numberOfColumns"));
			
			if (request.eventName.equals(eventName)) {
				VenueInfo venueInfo = new VenueInfo().setVenueName(venueName).setEventName(eventName).
				                                      setCapacity(capacity).setSeated(seated).
				                                      setNumberOfRows(numberOfRows).setNumberOfColumns(numberOfColumns);
				
				response.setAck(Ack.SUCCESS.toString()).setVenueInfo(venueInfo);
			} else {
				response.setAck(Ack.EVENT_NAME_UNKNOWN.toString());
			}
		} catch (IOException e) {
			LOGGER.severe(e.getMessage());
			response.setAck(Ack.ERROR.toString());
		}
		
		return response;
	}
	
}
