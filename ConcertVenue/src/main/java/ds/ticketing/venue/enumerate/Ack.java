package ds.ticketing.venue.enumerate;

public enum Ack {
	
	SUCCESS("SUCCESS"),
	EVENT_NAME_UNKNOWN("EVENT_NAME_UNKNOWN"),
	ERROR("ERROR");
	
	private final String name;
	
	private Ack(String name) {
		this.name = name;
	}
	
	public String toString() {
		return this.name;
	}
	
}
