package ds.ticketing.venue;

import ds.ticketing.venue.complex.GetVenueInfoRequestType;
import ds.ticketing.venue.complex.GetVenueInfoResponseType;

public interface ConcertVenue {
	
	public GetVenueInfoResponseType getVenueInfo(GetVenueInfoRequestType request);
	
}
