import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;


public class Main {
	
	/**
	 * @param args
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {
		Properties prop = new Properties();
		String connectionURL;
		
		Connection connection = null;
		Statement insertUserStatement = null;
		Statement selectUserStatement = null;
		ResultSet resultSet = null;
		
		int accountId;
		
		String firstName;
		
		String lastName;
		
		String email_addr;
		
		String province;
		
		double balance;
		
		String currency;
		
		
		
		try {
			
			prop.load(new FileInputStream("local_config.properties"));
			String hostname = prop.getProperty("hostname");
			connectionURL = "jdbc:derby:/home/" + hostname + "/eclipse/workspace/JiniBankDatabase";
			
			connection = DriverManager.getConnection(connectionURL);
			
			String selectUserQuery = "SELECT * from bankAccounts";
			selectUserStatement = connection.createStatement();
			resultSet = selectUserStatement.executeQuery(selectUserQuery);
			while(resultSet.next())
			{
				accountId=resultSet.getInt("accountId");
				firstName=resultSet.getString("firstName");
				lastName=resultSet.getString("lastName");
				email_addr=resultSet.getString("email_addr");
				province=resultSet.getString("province");
				balance=resultSet.getDouble("balance");
				currency=resultSet.getString("currency");
				
				System.out.println(accountId + " -- " + firstName + " -- "
						+ lastName + " -- " + email_addr + " -- " + province
						+ " -- " + balance + " -- " + currency);
				
			}
			
			/*
			String deleteQuery="delete from bankAccounts";
			Statement deleteUsersStatement=connection.createStatement();
			deleteUsersStatement.executeUpdate(deleteQuery);
			*/
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

}
