package ds.ticketing.organizer.bootstrap;

import java.rmi.RemoteException;

import ds.ticketing.organizer.BootstrapEventOrganizerResponseType;
import ds.ticketing.organizer.EventOrganizerImpl;
import ds.ticketing.organizer.EventOrganizerImplProxy;

public class BootstrapEventOrganizer {
	
	public static void main(String[] args) {
		try {
			EventOrganizerImplProxy eventOrganizerImplProxy = new EventOrganizerImplProxy();
			EventOrganizerImpl eventOrganizerImpl = eventOrganizerImplProxy.getEventOrganizerImpl();
			
			BootstrapEventOrganizerResponseType response = eventOrganizerImpl.bootstrapEventOrganizer();
			
			if (response.getAck().equals("SUCCESS")) {
				System.out.println("Successfully bootstrapped event organizer to its database and registered a bank account.");
			} else {
				System.out.println("Failed to bootstrap event organizer to its database and register a bank account.");
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
}
