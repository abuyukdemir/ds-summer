#!/bin/bash

function removeFile {
	if [ -f $1 ]
	then
		rm $1
	fi
}

function removeDirectory {
	if [ -d $1 ]
	then
		rm -r $1
	fi
}

configDir=../glassfish3122eclipsedefaultdomain/config

removeFile ${configDir}/local_config.properties
removeFile ${configDir}/venue_config.properties
removeFile ${configDir}/reseller_config.properties
removeFile ${configDir}/organizer_config.properties
removeFile ${configDir}/paypal_sdk_config.properties
removeFile ${configDir}/bank_config.properties

removeDirectory ../EventOrganizerDatabase
removeDirectory ../BankDatabase
