package ds.ticketing.reseller.trigger;

import java.rmi.RemoteException;

import ds.ticketing.reseller.TicketResellerImpl;
import ds.ticketing.reseller.TicketResellerImplProxy;
import ds.ticketing.reseller.TransferCollectedMoneyResponseType;

public class TransferCollectedMoney {
	
	public static void main(String[] args) {
		try {
			TicketResellerImplProxy TicketResellerImplProxy = new TicketResellerImplProxy();
			TicketResellerImpl TicketResellerImpl = TicketResellerImplProxy.getTicketResellerImpl();
			
			TransferCollectedMoneyResponseType response = TicketResellerImpl.transferCollectedMoney();
			
			if (response.getAck().equals("SUCCESS")) {
				System.out.println("Successfully transferred collected money (minus a fee) to the event organizer's bank account.");
			} else {
				System.out.println("Failed to transfer collected money (minus a fee) to the event organizer's bank account.");
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
}
