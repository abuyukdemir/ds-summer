package ds.ticketing.reseller.bootstrap;

import java.rmi.RemoteException;

import ds.ticketing.reseller.BootstrapTicketResellerResponseType;
import ds.ticketing.reseller.TicketResellerImpl;
import ds.ticketing.reseller.TicketResellerImplProxy;

public class BootstrapTicketReseller {
	
	public static void main(String[] args) {
		try {
			TicketResellerImplProxy TicketResellerImplProxy = new TicketResellerImplProxy();
			TicketResellerImpl TicketResellerImpl = TicketResellerImplProxy.getTicketResellerImpl();
			
			BootstrapTicketResellerResponseType response = TicketResellerImpl.bootstrapTicketReseller();
			
			if (response.getAck().equals("SUCCESS")) {
				System.out.println("Successfully registered a bank account for the ticket reseller.");
			} else {
				System.out.println("Failed to register a bank account for the ticket reseller.");
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
}
