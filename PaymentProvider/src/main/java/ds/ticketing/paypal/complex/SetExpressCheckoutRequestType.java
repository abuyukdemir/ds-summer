package ds.ticketing.paypal.complex;

public class SetExpressCheckoutRequestType {
	
	public TicketInfo[] reservedTickets;
	
	public String returnURL;
	
	public String cancelURL;
	
	public SetExpressCheckoutRequestType trim() {
		for (int i = 0; i < this.reservedTickets.length; ++i) {
			this.reservedTickets[i] = this.reservedTickets[i].trim();
		}
		this.returnURL = this.returnURL.trim();
		this.cancelURL = this.cancelURL.trim();
		return this;
	}
	
}
