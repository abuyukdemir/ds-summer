package ds.ticketing.paypal.complex;

import urn.ebay.apis.eBLBaseComponents.PaymentDetailsType;

public class GetExpressCheckoutDetailsResponseType {

	public String ack;
	
	public String token;
	
	public String payerId;
	
	public PaymentDetailsType[] paymentDetails;
	
	public GetExpressCheckoutDetailsResponseType setAck(String ack) {
		this.ack = ack;
		return this;
	}
	
	public GetExpressCheckoutDetailsResponseType setToken(String token) {
		this.token = token;
		return this;
	}
	
	public GetExpressCheckoutDetailsResponseType setPayerId(String payerId) {
		this.payerId = payerId;
		return this;
	}
	
	public GetExpressCheckoutDetailsResponseType setPaymentDetails(PaymentDetailsType[] paymentDetails) {
		this.paymentDetails = paymentDetails;
		return this;
	}
	
}
