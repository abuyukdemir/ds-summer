package ds.ticketing.paypal.complex;

public class GetExpressCheckoutDetailsRequestType {
	
	public String token;
	
	public String payerId;
	
	public GetExpressCheckoutDetailsRequestType trim() {
		this.token = this.token.trim();
		this.payerId = this.token.trim();
		return this;
	}
	
}
