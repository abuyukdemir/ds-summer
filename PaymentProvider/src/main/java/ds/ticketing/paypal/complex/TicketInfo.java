package ds.ticketing.paypal.complex;

public class TicketInfo {
	
	public int id;
	
	public String eventName;
	
	public String venueName;
	
	public String seatNumber;
	
	public double price;
	
	public String currency;
	
	public TicketInfo setId(int id) {
		this.id = id;
		return this;
	}
	
	public TicketInfo setEventName(String eventName) {
		this.eventName = eventName;
		return this;
	}
	
	public TicketInfo setVenueName(String venueName) {
		this.venueName = venueName;
		return this;
	}
	
	public TicketInfo setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
		return this;
	}
	
	public TicketInfo setPrice(double price) {
		this.price = price;
		return this;
	}
	
	public TicketInfo setCurrency(String currency) {
		this.currency = currency;
		return this;
	}
	
	public TicketInfo trim() {
		this.eventName = this.eventName.trim();
		this.venueName = this.venueName.trim();
		this.seatNumber = this.seatNumber.trim();
		this.currency = this.currency.trim();
		return this;
	}
	
}
