package ds.ticketing.paypal.complex;

public class SetExpressCheckoutResponseType {
	
	public String ack;
	
	public String token;
	
	public SetExpressCheckoutResponseType setAck(String ack) {
		this.ack = ack;
		return this;
	}
	
	public SetExpressCheckoutResponseType setToken(String token) {
		this.token = token;
		return this;
	}
	
}
