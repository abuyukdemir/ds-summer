package ds.ticketing.paypal.complex;

public class DoExpressCheckoutPaymentResponseType {
	
	public String ack;
	
	public DoExpressCheckoutPaymentResponseType setAck(String ack) {
		this.ack = ack;
		return this;
	}
	
}
