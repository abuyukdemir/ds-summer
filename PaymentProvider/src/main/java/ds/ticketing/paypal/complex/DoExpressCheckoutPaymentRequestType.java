package ds.ticketing.paypal.complex;

import urn.ebay.apis.eBLBaseComponents.PaymentDetailsType;

public class DoExpressCheckoutPaymentRequestType {
	
	public String token;
	
	public String payerId;
	
	public PaymentDetailsType[] paymentDetails;
	
	public DoExpressCheckoutPaymentRequestType trim() {
		this.token = this.token.trim();
		this.payerId = this.payerId.trim();
		return this;
	}
	
}
