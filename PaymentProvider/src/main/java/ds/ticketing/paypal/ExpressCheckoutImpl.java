package ds.ticketing.paypal;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import javax.jws.WebMethod;
import javax.jws.WebService;

import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentReq;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsReq;
import urn.ebay.api.PayPalAPI.PayPalAPIInterfaceServiceService;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutReq;
import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.CurrencyCodeType;
import urn.ebay.apis.eBLBaseComponents.DoExpressCheckoutPaymentRequestDetailsType;
import urn.ebay.apis.eBLBaseComponents.ErrorType;
import urn.ebay.apis.eBLBaseComponents.PaymentActionCodeType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsItemType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsType;
import urn.ebay.apis.eBLBaseComponents.SellerDetailsType;
import urn.ebay.apis.eBLBaseComponents.SetExpressCheckoutRequestDetailsType;
import ds.ticketing.paypal.complex.DoExpressCheckoutPaymentRequestType;
import ds.ticketing.paypal.complex.DoExpressCheckoutPaymentResponseType;
import ds.ticketing.paypal.complex.GetExpressCheckoutDetailsRequestType;
import ds.ticketing.paypal.complex.GetExpressCheckoutDetailsResponseType;
import ds.ticketing.paypal.complex.SetExpressCheckoutRequestType;
import ds.ticketing.paypal.complex.SetExpressCheckoutResponseType;
import ds.ticketing.paypal.complex.TicketInfo;
import ds.ticketing.paypal.enumerate.Ack;

@WebService
public class ExpressCheckoutImpl implements ExpressCheckout {
	
	private static final Logger LOGGER = Logger.getLogger(ExpressCheckoutImpl.class.getPackage().getName());
	
	@Override
	@WebMethod
	public SetExpressCheckoutResponseType setExpressCheckout(SetExpressCheckoutRequestType request) {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		request = request.trim();
		
		SetExpressCheckoutResponseType response = new SetExpressCheckoutResponseType();
		
		PaymentDetailsType paymentDetails = new PaymentDetailsType();
		List<PaymentDetailsItemType> paymentDetailsItems = new LinkedList<PaymentDetailsItemType>();
		
		double totalPrice = 0;
		
		for (TicketInfo reservingTicket : request.reservedTickets) {
			PaymentDetailsItemType paymentDetailsItem = new PaymentDetailsItemType();
			
			paymentDetailsItem.setName("Ticket " + reservingTicket.eventName + " " + reservingTicket.seatNumber);
			
			CurrencyCodeType currencyCode = CurrencyCodeType.fromValue(request.reservedTickets[0].currency);
			BasicAmountType basicAmount = new BasicAmountType(currencyCode, Double.toString(reservingTicket.price));
			paymentDetailsItem.setAmount(basicAmount);
			
			paymentDetailsItems.add(paymentDetailsItem);
			
			totalPrice += reservingTicket.price;
		}
		
		paymentDetails.setPaymentDetailsItem(paymentDetailsItems);
		
		CurrencyCodeType currencyCode = CurrencyCodeType.fromValue(request.reservedTickets[0].currency);
		BasicAmountType totalAmount = new BasicAmountType(currencyCode, Double.toString(totalPrice));
		paymentDetails.setOrderTotal(totalAmount);
		
		paymentDetails.setPaymentAction(PaymentActionCodeType.SALE);
		
		SellerDetailsType sellerDetails = new SellerDetailsType();
		try {
			Properties prop = new Properties();
			prop.load(new FileInputStream("paypal_sdk_config.properties"));
			String sandboxEmailAddress = prop.getProperty("sandbox.EmailAddress");
			sellerDetails.setPayPalAccountID(sandboxEmailAddress);
			paymentDetails.setSellerDetails(sellerDetails);
		} catch (IOException e) {
			LOGGER.severe(e.getMessage());
		}
		
		SetExpressCheckoutRequestDetailsType setExpressCheckoutRequestDetails = new SetExpressCheckoutRequestDetailsType();
		setExpressCheckoutRequestDetails.setPaymentDetails(Arrays.asList(paymentDetails));
		setExpressCheckoutRequestDetails.setReturnURL(request.returnURL);
		setExpressCheckoutRequestDetails.setCancelURL(request.cancelURL);
		
		urn.ebay.api.PayPalAPI.SetExpressCheckoutRequestType setExpressCheckoutRequest =
				new urn.ebay.api.PayPalAPI.SetExpressCheckoutRequestType(setExpressCheckoutRequestDetails);
		
		SetExpressCheckoutReq setExpressCheckoutReq = new SetExpressCheckoutReq();
		setExpressCheckoutReq.setSetExpressCheckoutRequest(setExpressCheckoutRequest);
		
		PayPalAPIInterfaceServiceService service = null;
		try {
			service = new PayPalAPIInterfaceServiceService("paypal_sdk_config.properties");
		} catch (IOException e) {
			LOGGER.severe("Error Message : " + e.getMessage());
			response.setAck(Ack.ERROR.toString());
		}
		
		urn.ebay.api.PayPalAPI.SetExpressCheckoutResponseType setExpressCheckoutResponseType = null;
		try {
			setExpressCheckoutResponseType = service.setExpressCheckout(setExpressCheckoutReq);
		} catch (Exception e) {
			LOGGER.severe("Error Message : " + e.getMessage());
			response.setAck(Ack.ERROR.toString());
		}
		
		if (setExpressCheckoutResponseType.getAck().getValue().equalsIgnoreCase("success")) {
			response.setAck(Ack.SUCCESS.toString()).setToken(setExpressCheckoutResponseType.getToken());
		} else {
			List<ErrorType> errorList = setExpressCheckoutResponseType.getErrors();
			LOGGER.severe("API Error Message : " + errorList.get(0).getLongMessage());
			response.setAck(Ack.ERROR.toString());
		}
		
		return response;
	}
	
	public GetExpressCheckoutDetailsResponseType getExpressCheckoutDetails(GetExpressCheckoutDetailsRequestType request) {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		request = request.trim();
		
		GetExpressCheckoutDetailsResponseType response = new GetExpressCheckoutDetailsResponseType();
		
		GetExpressCheckoutDetailsReq getExpressCheckoutDetailsReq = new GetExpressCheckoutDetailsReq();
		urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsRequestType getExpressCheckoutDetailsRequest =
				new urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsRequestType(request.token);
		getExpressCheckoutDetailsReq.setGetExpressCheckoutDetailsRequest(getExpressCheckoutDetailsRequest);
		
		PayPalAPIInterfaceServiceService service = null;
		try {
			service = new PayPalAPIInterfaceServiceService("paypal_sdk_config.properties");
		} catch (IOException e) {
			LOGGER.severe("Error Message : " + e.getMessage());
			response.setAck(Ack.ERROR.toString());
		}
		
		urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsResponseType getExpressCheckoutDetailsResponse = null;
		try {
			getExpressCheckoutDetailsResponse = service.getExpressCheckoutDetails(getExpressCheckoutDetailsReq);
		} catch (Exception e) {
			LOGGER.severe("Error Message : " + e.getMessage());
			response.setAck(Ack.ERROR.toString());
		}
		
		if (getExpressCheckoutDetailsResponse.getAck().getValue().equalsIgnoreCase("success")) {
			List<PaymentDetailsType> paymentDetails = getExpressCheckoutDetailsResponse.getGetExpressCheckoutDetailsResponseDetails().getPaymentDetails();
			
			response.setAck(Ack.SUCCESS.toString()).setToken(request.token).
			         setPayerId(getExpressCheckoutDetailsResponse.getGetExpressCheckoutDetailsResponseDetails().getPayerInfo().getPayerID()).
			         setPaymentDetails(paymentDetails.toArray(new PaymentDetailsType[paymentDetails.size()]));
		} else {
			List<ErrorType> errorList = getExpressCheckoutDetailsResponse.getErrors();
			LOGGER.severe("API Error Message : " + errorList.get(0).getLongMessage());
			response.setAck(Ack.ERROR.toString());
		}
		
		return response;
	}
	
	public DoExpressCheckoutPaymentResponseType doExpressCheckoutPayment(DoExpressCheckoutPaymentRequestType request) {
		LOGGER.info(Arrays.toString(Thread.currentThread().getStackTrace()));
		
		request = request.trim();
		
		DoExpressCheckoutPaymentResponseType response = new DoExpressCheckoutPaymentResponseType();
		
		DoExpressCheckoutPaymentReq doExpressCheckoutPaymentReq = new DoExpressCheckoutPaymentReq();
		DoExpressCheckoutPaymentRequestDetailsType doExpressCheckoutPaymentRequestDetails = new DoExpressCheckoutPaymentRequestDetailsType();
		
		doExpressCheckoutPaymentRequestDetails.setToken(request.token);
		doExpressCheckoutPaymentRequestDetails.setPayerID(request.payerId);
		doExpressCheckoutPaymentRequestDetails.setPaymentDetails(Arrays.asList(request.paymentDetails));
		doExpressCheckoutPaymentRequestDetails.setPaymentAction(PaymentActionCodeType.SALE);
		
		urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentRequestType doExpressCheckoutPaymentRequest =
				new urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentRequestType(doExpressCheckoutPaymentRequestDetails);
		doExpressCheckoutPaymentReq.setDoExpressCheckoutPaymentRequest(doExpressCheckoutPaymentRequest);
		
		PayPalAPIInterfaceServiceService service = null;
		try {
			service = new PayPalAPIInterfaceServiceService("paypal_sdk_config.properties");
		} catch (IOException e) {
			LOGGER.severe("Error Message : " + e.getMessage());
			response.setAck(Ack.ERROR.toString());
		}
		
		urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentResponseType doExpressCheckoutPaymentResponse = null;
		try {
			doExpressCheckoutPaymentResponse = service.doExpressCheckoutPayment(doExpressCheckoutPaymentReq);
		} catch (Exception e) {
			LOGGER.severe("Error Message : " + e.getMessage());
			response.setAck(Ack.ERROR.toString());
		}
		
		if (doExpressCheckoutPaymentResponse.getAck().getValue().equalsIgnoreCase("success")) {
			response.setAck(Ack.SUCCESS.toString());
		} else {
			List<ErrorType> errorList = doExpressCheckoutPaymentResponse.getErrors();
			LOGGER.severe("API Error Message : " + errorList.get(0).getLongMessage());
			response.setAck(Ack.ERROR.toString());
		}
		
		return response;
	}
	
}
