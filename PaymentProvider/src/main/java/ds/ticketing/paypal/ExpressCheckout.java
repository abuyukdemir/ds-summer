package ds.ticketing.paypal;

import ds.ticketing.paypal.complex.DoExpressCheckoutPaymentRequestType;
import ds.ticketing.paypal.complex.DoExpressCheckoutPaymentResponseType;
import ds.ticketing.paypal.complex.GetExpressCheckoutDetailsRequestType;
import ds.ticketing.paypal.complex.GetExpressCheckoutDetailsResponseType;
import ds.ticketing.paypal.complex.SetExpressCheckoutRequestType;
import ds.ticketing.paypal.complex.SetExpressCheckoutResponseType;

public interface ExpressCheckout {
	
	public SetExpressCheckoutResponseType setExpressCheckout(SetExpressCheckoutRequestType request);
	
	public GetExpressCheckoutDetailsResponseType getExpressCheckoutDetails(GetExpressCheckoutDetailsRequestType request);
	
	public DoExpressCheckoutPaymentResponseType doExpressCheckoutPayment(DoExpressCheckoutPaymentRequestType request);
	
}
