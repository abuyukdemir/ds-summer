package ds.ticketing.paypal.enumerate;

public enum Ack {
	
	SUCCESS("SUCCESS"),
	ERROR("ERROR");
	
	private final String name;
	
	private Ack(String name) {
		this.name = name;
	}
	
	public String toString() {
		return this.name;
	}
	
}
