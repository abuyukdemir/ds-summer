Designing the web services                                  

    -.  There are 5 main web services: Bank, ConcertVenue, EventOrganizer,
        PaymentProvider and TicketReseller.
    -.  Each of these web services are dynamic web projects, built using
        Maven resolving classpath issues when deploying onto different
        computers.
    -.  A Bank web service enables users to register a bank account, retrieve
        the current balance and make payments (possibly between different
        accounts). A Bank web service is connected to a local database of bank
        accounts, whom only he has access to.
    -.  A ConcertVenue web service enables an event organizer to fetch
        information from a particular venue. A concert venue is identified by
        its venue_config.properties configuration file.
    -.  An EventOrganizer web service is connected to a local database of
        tickets, whom only he has access to. An event organizer is identified
        by its organizer_config.properties configuration file.
    -.  A PaymentProvider web service implements wrappers around the PayPal
        SetExpressCheckout, GetExpressCheckoutDetails and DoExpressCheckOut
        SOAP methods.
    -.  A TicketReseller forwards user requests to the EventOrganizer and
        includes features for aborting and finalizing the ticket buying process.



Derby database                                     
                                                            

    -.  Derby comes preinstalled with GlassFish and enables us to access a SQL
        database via the JDBC API.
    -.  For the assignment on transactions, we used the power of this JDBC API
        to integrate atomic transactions and to prevent dirty reads.
    -.  We also used the JDBC API's capability to batch SQL queries reducing
        round-trip times.
    -.  In derby_example_usage you can find the statements we used to create
        our tables.



Front end                                                  

    -.  We refer to TicketReseller/src/main/webapp.
    -.  The front-end is developed using HTML5, CSS3 (Twitter Bootstrap), JSP
        and JavaScript (jQuery). However, only the ticketing service has a web
        front-end deployed. The Jini part not
    -.  jQuery is used to validate forms and to make asynchronous postback
        calls to web services, such that the page does not need to be reloaded
        again.
    -.  There is no need to set up an external server, as GlassFish is capable
        of deploying .jsp files by itself.



Other completed features                                   

    -.  For each province in Belgium you can configure the maximum amount of
        tickets to be sold.
    -.  It is possible to configure the number of tickets that must be held at
        the entrance of the festival.
    -.  A bank logs user payments to a file.
    -.  You can have a look at the different properties files to get more
        insight at the different parameters we allow to be configured.
    -.  When a user reserves a ticket, no other user is able to reserve this
        ticket, unless the first user aborts his transaction thereby releasing
        the ticket.
    -.  TransferCollectedMoney lets a ticket reseller deposit the collected
        money (minus a preconfigured fee) to the event organizer's bank account.



Features that were not completed
    -.  A rate limiting policy.
    -.  A time-out mechanism for reserved tickets.



Jini                                                

    -.  This part of the assignment does not deploy a web front end.
    -.  Jini Auction functionality and JiniBankDatabase Service are working together.
    -.  We refer to README.Jini.txt for a more detailed description.



How to run?                                                

    -.  Configure local_config.properties on each machine.
    -.  Execute config.sh. This command copies the different configuration files
        to the working directory of deployed web services in GlassFish.
    -.  Import Bank, ConcertVenue and PaymentProvider projects in Eclipse and
        run them on the server.
    -.  Import EventOrganizer project in Eclipse and subsequently wsimport
        Bank and ConcertVenue web services, and run it on the server.
    -.  Import TicketReseller project in Eclipse and wsimport Bank and Event
        Organizer web services, and run it on the server.
    -.  Execute the commands
            wsimport -keep -p ds.ticketing.frontend.reseller <WSDL endpoint>
                -d WEB-INF/classes
        and
            wsimport ds.ticketing.frontend.paypal <WSDL endpoint>
                -d WEB-INF/classes
        in the webapp folder of TicketReseller linking the front end to these
        web services.
    -.  Execute BootstrapEventOrganizer.java to trigger the ticket generating
        process and to let an event organizer register a bank account.
    -.  Execute BootstrapTicketReseller to let a ticket reseller register a
        bank account.
    -.  Each TicketReseller being deployed should now be accessible from the
        outside using the TicketReseller's IP address.
