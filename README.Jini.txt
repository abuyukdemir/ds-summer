README Part For JINI

There can be multiple registering services ( so for each computer there should be a http deamon )
In order to start http deamon( classserver ) and all the other serives , put the river directory under dir: ~/Downloads/.
In the whole auction system, there will be only 1 lookupservice, 1 transaction service and 1 javaspace service.

First of all, we run httpd. To do this, use cd command from terminal to go to directory river/examples/hello.
while in this directory, use this command;

". scripts/httpd.sh"
This is enough to start the deamon.
Again; for each computer this deamon should be started.

Then select one computer as candidate to carry the other serives' responsibilities.
Start the services in this order of commands ( again in the dir : river/examples/hello)
". scripts/jrmp-reggie.sh"
". scripts/mahalo.sh"
". scripts/outrigger.sh"

After all these services are started successfully, then we can run auction services which auction clients can use.
Unfortunately we couldn't make an interface so, it is still in code state.
However all functionalities of javaspace, transactions and JINI services can be used an applied to our program.
In the implementation we have done, service creates a ticket and put it in the javaspace for auction( with some ticket info and beginning price)
Clients can see the service and find the corresponding ticket which this service is auctioning. It can't see the other tickets because it can find a ticket in 
javaspace with a special string which is uniquely identifying the Auction Service.

By using the clients functon Bid, and waitForChange, it can bid to the ticket and be notified by the changes.
In the current main function of the Client class, there is an example scenario which shows how the auction works.

Package: org.ds.util is a package where the connection to javapspace and transaction service is handled. It is caching the current javaspace and transaction service,
and use it that way. This packaged is referenced from www.softwarematter.com. ( This package itself has nothing to do with Auction Mechanism)

How the auction process works:

1- Auction service starts and registers itself to lookup service. 
2- Auction service writes the ticket to the javaspace.
3- In this point, Auction Service can be stopped. 
4- From now on Clients can join the any auction service and find that auction service's ticket in the javaspace.
5- The cost of joining the auction is 10. ( When a new user enters, it increases the price by 10)
6- After each bidding, if nobody bids for 1 minute, then the ticket is sold, so it is removed from javaspace.
7- Multiple users can compete for the same ticket.
8- After ticket is deleted from javaspace( when the auction ends) , winner client is given a alphanumeric code which he can use to purchase the ticket.


It is simple and working example of JINI and JavaSpaces.
* In order to chose which service client should use serviceIndexes integer list. (0) shows the first service which is available.
* The implementation can be definetly more advanced then this. Currently there is no interface so client main function should be run partially in order to
  see the services and then connect.

IMPORTANT: (IN THE CODE )the policy.all file directory and the address in order to maintain a connection between clients and lookup service, should be CHANGED because of the fact that they can be deployed in any computer under any hostname.
